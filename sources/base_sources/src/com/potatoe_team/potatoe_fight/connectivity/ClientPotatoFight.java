package com.potatoe_team.potatoe_fight.connectivity;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;

import com.potatoe_team.potatoe_fight.manager.ConnectivityManager;

public class ClientPotatoFight implements Runnable{

	private static final int SERVER_PORT = 1030;
	private boolean connected = false; 
	private Socket server_socket;
	
	public ClientPotatoFight() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		
		this.joinServer();
		if(connected){
			this.game();
		}
		
	}

	private void joinServer() {
		ConnectivityManager.getInstance().wifi_manager.requestConnectionInfo(ConnectivityManager.getInstance().channel, new ConnectionInfoListener() {
		    @Override
		    public void onConnectionInfoAvailable(WifiP2pInfo p2pInfo) {
		        if (!p2pInfo.isGroupOwner) {
		            // Joined group as client - connect to GO
		            server_socket = new Socket();
		            try {
						server_socket.connect(new InetSocketAddress(p2pInfo.groupOwnerAddress, SERVER_PORT));
						connected = true;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
		    }
		});
		
	}
	
	private void game(){
		this.game_init();
		this.game_run();
	}
	
	private void game_init(){
		
	}
	
	private void send_player_move() {
		// TODO Auto-generated method stub
		
	}

	private void send_player_ready() {
		// TODO Auto-generated method stub
		
	}

	private void send_player_info(Object player_info) {
		// TODO Auto-generated method stub
		
	}

	private Object send_player_attack(Object attack) {
		// TODO Auto-generated method stub
		return null;
	}

	private void send_player_change_weapon(int id) {
		// TODO Auto-generated method stub
		
	}

	private void send_player_die() {
		// TODO Auto-generated method stub
		
	}

	private void game_run(){
		boolean running = true;
		
		while(running){
			
	        running = false;       
		}
	}


}
