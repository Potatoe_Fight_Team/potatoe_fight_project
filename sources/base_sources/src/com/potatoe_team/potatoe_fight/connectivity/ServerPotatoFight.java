package com.potatoe_team.potatoe_fight.connectivity;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

import android.net.wifi.p2p.WifiP2pManager;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.potatoe_team.potatoe_fight.manager.ConnectivityManager;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.map.Map;
import com.potatoe_team.potatoe_fight.map.MapXMLData;

public class ServerPotatoFight implements Runnable {

	private static final int SERVER_PORT = 1030;
	private ArrayList<InetAddress> clients = new ArrayList<InetAddress>();
	
	public ServerPotatoFight()  {
		
	}

	@Override
	public void run() {
		this.create_group();
		try {
			this.startServer();
			this.game();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void create_group(){
		
		ConnectivityManager.getInstance().wifi_manager.createGroup(ConnectivityManager.getInstance().channel,new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {

            	Toast.makeText(ResourcesManager.getInstance().activity, "Group Created",Toast.LENGTH_SHORT).show();

            }

           @Override
           public void onFailure(int reason) {
        	   Toast.makeText(ResourcesManager.getInstance().activity, "Group Creation failed",Toast.LENGTH_SHORT).show();
           }                
        });
	}
	
	private void startServer() throws IOException {
	    clients.clear();
	    ServerSocket serverSocket = new ServerSocket(SERVER_PORT);

	    // Collect client ip's
	    while(clients.size() == 2) {
	       Socket clientSocket = serverSocket.accept();
	       clients.add(clientSocket.getInetAddress());
	       clientSocket.close();
	    }
	}
	
	private void game(){
		this.game_init();
		this.game_run();
	}
	
	private void game_init(){
		int[] id_list = MapXMLData.maps_id_list();
		Random r = new Random();
		int i = r.nextInt(id_list.length);
		int id = id_list[i];
		Map map = MapXMLData.load_mapXMLData(id);
		int s1 = r.nextInt(map.spawnPoints.size());
		int s2;
		do{
			s2 = r.nextInt(map.spawnPoints.size());
		}while(s1 == s2);
		Vector2 p1 = map.spawnPoints.get(s1);
		Vector2 p2 = map.spawnPoints.get(s2);
		for(InetAddress client : clients){
			this.send_map_id(client, id);
		}
		
		this.send_player_spawnpoint(clients.get(0), p1);
		this.send_player_spawnpoint(clients.get(1), p2);
		
		this.send_player_info(clients.get(0), this.ask_player_info(clients.get(1)));
		this.send_player_info(clients.get(1), this.ask_player_info(clients.get(0)));
		
		for(InetAddress client : clients){
			this.ask_ready(client);
		}
		
		for(InetAddress client : clients){
			this.send_start(client);
		}
	}
	
	private void send_start(InetAddress client) {
		// TODO Auto-generated method stub
		
	}

	private void ask_ready(InetAddress client) {
		// TODO Auto-generated method stub
		
	}

	private void send_player_info(InetAddress inetAddress, Object ask_player_info) {
		// TODO Auto-generated method stub
		
	}

	private Object ask_player_info(InetAddress inetAddress) {
		// TODO Auto-generated method stub
		return null;
	}

	private void send_player_spawnpoint(InetAddress inetAddress, Vector2 p1) {
		// TODO Auto-generated method stub
		
	}

	private void send_map_id(InetAddress client, int id) {
		// TODO Auto-generated method stub
		
	}

	private void game_run(){
		boolean running = true;
		
		while(running){
			
	        running = false;       
		}
	}

}
