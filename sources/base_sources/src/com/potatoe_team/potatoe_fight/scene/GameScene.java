package com.potatoe_team.potatoe_fight.scene;

/**
 * Don't use this
 * @author Guillaume
 * @author Alban
 * @version 1.0
 */
/*public class GameScene extends BaseScene {
	
	private Map map;
	private Entity backLayer;
	private Entity midLayer;
	private Entity foreLayer;
	
	
	@Override
	public void createScene() {
		
		map = new Map ();
		backLayer = new Entity();
		midLayer = new Entity();
		foreLayer = new Entity();
		createBackground();
		createMidground();
		createForeground(); 
		attachChilds();
	}

	private void createBackground() {
		
		map.setBackground(new Sprite(400, 240, resourcesManager.game_background_region, vbom) {
	        @Override
	        protected void preDraw(GLState pGLState, Camera pCamera) {
	            
	        	super.preDraw(pGLState, pCamera);
	            pGLState.enableDither();
	        }
	    });
	}
	
	private void createMidground() {
		
		map.setMidground(new Sprite(400, 100, 800, 200, resourcesManager.game_midground_region, vbom) {
	        @Override
	        protected void preDraw(GLState pGLState, Camera pCamera) {
	        	
	            super.preDraw(pGLState, pCamera);
	            pGLState.enableDither();
	        }
	    });
	}
	
	private void attachChilds () {
		
		attachChild(backLayer);
		attachChild(midLayer);
		attachChild(foreLayer);
		//backLayer.attachChild(map.getBackground());
		midLayer.attachChild(map.getMidground());
	}
	
	public void attachItemsToMidground(Sprite sprite) {
		
		midLayer.attachChild(sprite);
	}
	
	private void createForeground() {
		
		// generate the HUB
	}
	
	@Override
	public void onBackKeyPressed() {
		
		System.exit(0);		
	}

	@Override
	public SceneType getSceneType() {
		
		return SceneType.SCENE_GAME;
	}

	@Override
	public void disposeScene() {
		
		//map.getBackground().detachSelf();
		//map.getBackground().dispose();
		map.getMidground().detachSelf();
		map.getMidground().dispose();
	    this.detachSelf();
	    this.dispose();
	}
}*/