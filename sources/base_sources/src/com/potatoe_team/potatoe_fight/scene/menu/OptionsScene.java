package com.potatoe_team.potatoe_fight.scene.menu;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

import com.potatoe_team.potatoe_fight.manager.SceneManager;
import com.potatoe_team.potatoe_fight.manager.SceneManager.SceneType;
import com.potatoe_team.potatoe_fight.scene.BaseScene;

public class OptionsScene extends BaseScene implements OnClickListener{
	private ButtonSprite back;
	private ButtonSprite sound;
	private ButtonSprite music;
	private ButtonSprite sound_deac;
	private ButtonSprite music_deac;
	private boolean bMusic=true,bSound=true;
	@Override
	public void createScene() {
		createBackground();
		createChildScene();
	}

	private void createChildScene() {
		
		back=new ButtonSprite(80, 450, resourcesManager.return_region , vbom,this);
		sound=new ButtonSprite(400,300 , resourcesManager.button_region , vbom,this);
		music=new ButtonSprite(400, 200, resourcesManager.button_region , vbom,this);
		sound_deac=new ButtonSprite(400, 300, resourcesManager.no_sound_region , vbom,this);
		music_deac=new ButtonSprite(400, 200, resourcesManager.no_sound_region , vbom,this);
		attachChild(back);
		attachChild(sound);
		attachChild(music);
		this.registerTouchArea(back);
		this.registerTouchArea(sound);
		this.registerTouchArea(music);
	}
	public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
                float pTouchAreaLocalY) {
		if(pButtonSprite==back){
			SceneManager.getInstance().backToMenuScene();
			SceneManager.getInstance().disposeOptionsScene();
		}
		if(pButtonSprite==music){
			bMusic=false;
			this.unregisterTouchArea(music);
			detachChild(music);
			attachChild(music_deac);
			this.registerTouchArea(music_deac);
		}
		if(pButtonSprite==sound){
			bSound=false;
			this.unregisterTouchArea(sound);
			detachChild(sound);
			attachChild(sound_deac);
			this.registerTouchArea(sound_deac);
		}
		if(pButtonSprite==music_deac){
			bMusic=true;
			this.unregisterTouchArea(music_deac);
			detachChild(music_deac);
			attachChild(music);
			this.registerTouchArea(music);
		}
		if(pButtonSprite==sound_deac){
			bSound=true;
			this.unregisterTouchArea(sound_deac);
			detachChild(sound_deac);
			attachChild(sound);
			this.registerTouchArea(sound);
		}
	}
	private void createBackground() {
		
		attachChild(new Sprite(400, 240, resourcesManager.menu_background_region, vbom)
	    {
	        @Override
	        protected void preDraw(GLState pGLState, Camera pCamera) 
	        {
	            super.preDraw(pGLState, pCamera);
	            pGLState.enableDither();
	        }
	    });
	}

	@Override
	public void onBackKeyPressed() {
		SceneManager.getInstance().backToMenuScene();
		SceneManager.getInstance().disposeOptionsScene();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_OPTIONS;
	}

	@Override
	public void disposeScene() {
	    this.detachSelf();
	    this.dispose();
	}
	

}
