package com.potatoe_team.potatoe_fight.scene.menu;


import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

import org.jdom2.Element;

import android.util.Log;

import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.manager.SceneManager;
import com.potatoe_team.potatoe_fight.manager.SceneManager.SceneType;
import com.potatoe_team.potatoe_fight.scene.BaseScene;

public class ProfilScene extends BaseScene implements IOnMenuItemClickListener {
	
	private MenuScene profil;
	private final int MENU_BACK = 0;
	private final int MENU_LEFT = 1;
	private final int MENU_RIGHT = 2;
	
	private Sprite back;
	private ButtonSprite gauche,droit;
	private Sprite potatoe1, potatoe2, prof;
	private int i;
	private ArrayList<Sprite> potatoe_list;

	@Override
	public void createScene() {
		
		profil = new MenuScene(ResourcesManager.getInstance().camera);
		profil.setPosition(0, 0);
		
		createBackground();
		createChildScene();
	}

	private void createChildScene() {
		
		potatoe_list = new ArrayList<Sprite>();
		potatoe1 = new Sprite(400, 240, resourcesManager.potatoe1_region, vbom);
		potatoe2 = new Sprite(400, 240, resourcesManager.potatoe2_region, vbom);
		
		final IMenuItem menuBack = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_BACK, resourcesManager.button_region, vbom),0.8f, 0.9f);
		final IMenuItem menuLeft = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_LEFT, resourcesManager.gauche_region, vbom),0.8f, 0.9f);
		final IMenuItem menuRight = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_RIGHT, resourcesManager.droit_region, vbom),0.8f, 0.9f);
		
		potatoe_list.add(potatoe1);
		potatoe_list.add(potatoe2);
		
		profil.buildAnimations();
	    profil.setBackgroundEnabled(false);
		
		profil.addMenuItem(menuBack);
		profil.addMenuItem(menuLeft);
		profil.addMenuItem(menuRight);
		
		menuBack.setPosition(100, 445);
		menuLeft.setPosition(120, 240);
		menuRight.setPosition(680, 240);
		
		i = resourcesManager.activity.getPictureID() - 1;
		
		potatoe1.setScale(0.6f);
		potatoe2.setScale(0.6f);
		
		profil.attachChild(potatoe_list.get(i));
		
		profil.setOnMenuItemClickListener(this);
		setChildScene(profil);
	}
	
	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {
		switch(pMenuItem.getID()){
		 	case MENU_BACK:
		 		SceneManager.getInstance().backToMenuScene();
				SceneManager.getInstance().disposeProfilScene();
	            return true;
		 	case MENU_LEFT:
		 		profil.detachChild(potatoe_list.get(i));
		 		if(i != 0){
					i--;
				}
				else{
					i = potatoe_list.size()-1;
				}
				profil.attachChild(potatoe_list.get(i));
				resourcesManager.activity.setPictureID(i % 2 +1);
	            return true;
		 	case MENU_RIGHT:
		 		profil.detachChild(potatoe_list.get(i));
		 		if(i != potatoe_list.size()-1) {
					i++;
				}
				else {
					i = 0;
				}
				profil.attachChild(potatoe_list.get(i));
				resourcesManager.activity.setPictureID(i % 2 +1);
	            return true;
        default:
            return false;
        }		
	}
	
	private void createBackground() {
		
		this.profil.attachChild(new Sprite(400, 240, resourcesManager.menu_background_region, vbom)
	    {
	        @Override
	        protected void preDraw(GLState pGLState, Camera pCamera) 
	        {
	            super.preDraw(pGLState, pCamera);
	            pGLState.enableDither();
	        }
	    });
	}

	@Override
	public void onBackKeyPressed() {
		
		SceneManager.getInstance().backToMenuScene();
		SceneManager.getInstance().disposeProfilScene();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_PROFIL;
	}

	@Override
	public void disposeScene() {
	    this.detachSelf();
	    this.dispose();
	}
}
