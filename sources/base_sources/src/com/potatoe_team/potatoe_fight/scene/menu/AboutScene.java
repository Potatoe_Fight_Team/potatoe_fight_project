package com.potatoe_team.potatoe_fight.scene.menu;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.util.GLState;

import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.manager.SceneManager;
import com.potatoe_team.potatoe_fight.manager.SceneManager.SceneType;
import com.potatoe_team.potatoe_fight.scene.BaseScene;

public class AboutScene extends BaseScene implements IOnMenuItemClickListener {
	
	private MenuScene about;
	private final int MENU_BACK = 0;
	private final int ABOUT_TEXT = 1;

	private Text text;
	
	@Override
	public void createScene() {
		
		about = new MenuScene(ResourcesManager.getInstance().camera);
		about.setPosition(0, 0);
		
		createBackground();
		createChildScene();
	}

	private void createChildScene() {
		
		final IMenuItem menuBack = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_BACK, resourcesManager.return_region, vbom),0.8f, 0.9f);
		
		text = new Text(400, 240, resourcesManager.font , 
				"PotatoFight is a fun multiplayer game where you control the potato of your choice \n"
				+"versus another player in epic fight over wifi direct \n"
				+"(you don't need anything else than two android 4.x devices with PotatoFight installed !)."
				+"\n\nAuthors:\n"
				+"– Rothstein Billy\n"
				+"– Willmann Lionel:\n"
				+"– Liu Xiaoyi\n"
				+"– Krauth Alban\n"
				+"– Rodrigues Guillaume\n\n"
				+"This game is part of a study project.", 
				vbom);
		about.attachChild(text);
		
		about.buildAnimations();
	    about.setBackgroundEnabled(false);
		
		about.addMenuItem(menuBack);		
		menuBack.setPosition(100, 445);

		about.setOnMenuItemClickListener(this);
		setChildScene(about);
	}

	private void createBackground() {
		
		this.about.attachChild(new Sprite(400, 240, resourcesManager.menu_background_region, vbom)
	    {
	        @Override
	        protected void preDraw(GLState pGLState, Camera pCamera) 
	        {
	            super.preDraw(pGLState, pCamera);
	            pGLState.enableDither();
	        }
	    });
	}

	@Override
	public void onBackKeyPressed() {
		SceneManager.getInstance().backToMenuScene();
		SceneManager.getInstance().disposeAboutScene();
	}

	@Override
	public SceneType getSceneType() {
		
		return SceneType.SCENE_ABOUT;
	}

	@Override
	public void disposeScene() {
	    this.detachSelf();
	    this.dispose();
	}

	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {
		switch(pMenuItem.getID()){
	 	case MENU_BACK:
	 		SceneManager.getInstance().backToMenuScene();
			SceneManager.getInstance().disposeAboutScene();
            return true;
	    default:
	        return false;
	    }		
	}
	

}