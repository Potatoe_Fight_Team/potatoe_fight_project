package com.potatoe_team.potatoe_fight.scene;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

import com.potatoe_team.potatoe_fight.manager.SceneManager;
import com.potatoe_team.potatoe_fight.manager.SceneManager.SceneType;

/**
 * 
 * @author billy,xiaoyi
 * @version 1.0
 */
public class MainMenuScene extends BaseScene implements IOnMenuItemClickListener  {

	private MenuScene menuChildScene;
	private final int MENU_PROFIL = 0;
	private final int MENU_SETTINGS = 1;
	private final int MENU_SINGLE = 2;
	private final int MENU_MULTI = 3;
	private final int MENU_RETURN = 4;
	private final int MENU_ABOUT = 5;
	private final int MENU_SOUND = 6;
	
	private boolean settingActivate = false;
	private boolean soundActivate = true;
	private IMenuItem soundMenuItem;
    private IMenuItem noSoundMenuItem;
	
	@Override
	public void createScene() {
		
		menuChildScene = new MenuScene(camera);
	    menuChildScene.setPosition(0, 0);
	    
		createBackground();
		createMenuChildScene();
	}

	@Override
	public void onBackKeyPressed() {
		
		resourcesManager.activity.finish();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_MENU;
	}

	@Override
	public void disposeScene() {
		menuChildScene.detachSelf();
		menuChildScene.dispose();
	    this.detachSelf();
	    this.dispose();
	}
	

	private void createBackground()
	{
	    this.menuChildScene.attachChild(new Sprite(400, 240, resourcesManager.menu_background_region, vbom)
	    {
	        @Override
	        protected void preDraw(GLState pGLState, Camera pCamera) 
	        {
	            super.preDraw(pGLState, pCamera);
	            pGLState.enableDither();
	        }
	    });
	}

	private void createMenuChildScene() {
		
	    final IMenuItem profilMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_PROFIL, resourcesManager.button_region, vbom),0.8f, 0.9f);
	    final IMenuItem settingsMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_SETTINGS, resourcesManager.settings_region, vbom), 0.8f, 0.9f);
	    final IMenuItem multiMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_MULTI, resourcesManager.play_region, vbom),0.8f, 0.9f);
	    final IMenuItem aboutMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_ABOUT, resourcesManager.about_region, vbom), 0.8f, 0.9f);
	    soundMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_SOUND, resourcesManager.sound_region, vbom), 0.8f, 0.9f);
	    noSoundMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_SOUND, resourcesManager.no_sound_region, vbom), 0.8f, 0.9f);
	   
	    menuChildScene.addMenuItem(profilMenuItem);
	    menuChildScene.addMenuItem(settingsMenuItem);
	    menuChildScene.addMenuItem(multiMenuItem);
	    menuChildScene.addMenuItem(aboutMenuItem);
	    menuChildScene.addMenuItem(soundMenuItem);
	    menuChildScene.addMenuItem(noSoundMenuItem);

	    menuChildScene.buildAnimations();
	    menuChildScene.setBackgroundEnabled(false);
	  
	    multiMenuItem.setPosition(profilMenuItem.getX()-100, profilMenuItem.getY()-300);
	    settingsMenuItem.setPosition(740, 410);
	    aboutMenuItem.setPosition(60, 410);
	    profilMenuItem.setPosition(profilMenuItem.getX()+100, profilMenuItem.getY()-300);
	    
	    soundMenuItem.setPosition(740, 340);
	    noSoundMenuItem.setPosition(740, 340);
	    soundMenuItem.setVisible(false);
	    noSoundMenuItem.setVisible(false);
	    
	    menuChildScene.setOnMenuItemClickListener(this);
	    setChildScene(menuChildScene);
	}

	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {
		switch(pMenuItem.getID()){
		 	case MENU_PROFIL:
		 		SceneManager.getInstance().createProfilScene();
	            return true;
	        case MENU_SETTINGS:
	        	if(settingActivate) {
	        		settingActivate = false;
	        		if(soundActivate) {	        			
	        			soundMenuItem.setVisible(false);
	        		}
	        		else {
	        		    noSoundMenuItem.setVisible(false);
	        		}        		
	        	}
	        	else {
	        		settingActivate = true;
	        		if(soundActivate) {
	        			soundMenuItem.setVisible(true);
	        		}
	        		else {
	        		    noSoundMenuItem.setVisible(true);
	        		}
	        	}
	            return true;
	        case MENU_SINGLE:
	        	//SceneManager.getInstance().createSinglePlayerMenuScene();
	        	SceneManager.getInstance().createGameScene(-1);
	            return true;
	        case MENU_MULTI:
	        	//SceneManager.getInstance().createMultiPlayerMenuScene();
	        	SceneManager.getInstance().createGameScene(-1);
	            return true;
	        case MENU_RETURN:
	        	System.exit(0);
	            return true;
	        case MENU_ABOUT:
	        	SceneManager.getInstance().createAboutMenuScene();
	            return true;
	        case MENU_SOUND:
	        	if(soundMenuItem.isVisible()) {
	        		soundMenuItem.setVisible(false);
	        		noSoundMenuItem.setVisible(true);
	        		soundActivate = false;
	        	}
	        	else {
	        		noSoundMenuItem.setVisible(false);
	        		soundMenuItem.setVisible(true);
	        		soundActivate = true;
	        	}
	            return true;
        default:
            return false;
        }		
	}	

}


