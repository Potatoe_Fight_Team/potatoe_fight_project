package com.potatoe_team.potatoe_fight.scene;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl.IAnalogOnScreenControlListener;
import org.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.extension.debugdraw.DebugRenderer;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.util.Vector2Pool;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.PinchZoomDetector;
import org.andengine.input.touch.detector.PinchZoomDetector.IPinchZoomDetectorListener;
import org.andengine.util.adt.align.HorizontalAlign;

import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.potatoe_team.potatoe_fight.manager.SceneManager;
import com.potatoe_team.potatoe_fight.manager.SceneManager.SceneType;
import com.potatoe_team.potatoe_fight.map.GameContactListener;
import com.potatoe_team.potatoe_fight.map.GroundBlock;
import com.potatoe_team.potatoe_fight.map.Map;
import com.potatoe_team.potatoe_fight.map.MiniMap;
import com.potatoe_team.potatoe_fight.map.WindBlowingManager;
import com.potatoe_team.potatoe_fight.map.WindBlowingManager.WindDirection;
import com.potatoe_team.potatoe_fight.player.Patate;
import com.potatoe_team.potatoe_fight.user_data.PlayerData;
import com.potatoe_team.potatoe_fight.utils.ButtonAnimatedSprite;
import com.potatoe_team.potatoe_fight.utils.HealthBar;
import com.potatoe_team.potatoe_fight.utils.WeaponSelectionArea;
import com.potatoe_team.potatoe_fight.weapon.Explosion;
import com.potatoe_team.potatoe_fight.weapon.WeaponList.WeaponType;

/**
 * @author billy, Rio
 */
public class MainGameScene extends BaseScene implements IOnSceneTouchListener,
		IPinchZoomDetectorListener {
	
	public enum GameMode {
		NORMAL,
		TIME_ATTACK,
	}

	private float MAX_ZOOM_FACTOR = 1.2f;
	private float MIN_ZOOM_FACTOR;
	public float WIDTH;
	public float HEIGHT;

	private Map map;
	private Entity backLayer, midLayer;

	public FixedStepPhysicsWorld mPhysicsWorld;
	public Body groundBody;
	public boolean isJumping = false;

	private Patate perso;
	private HUD gameHUD;
	private AnalogOnScreenControl leftAnalogOnScreenControl;
	private AnalogOnScreenControl rightAnalogOnScreenControl;
	private TiledSprite buttonA;
	private TiledSprite buttonB;
	private TiledSprite flag;
	private Sprite buttonSetting;
	private PinchZoomDetector pinchZoomDetector;
	private float initialTouchZoomFactor;
	private HealthBar healthBar;
	private String scoreText;
	private String timeText = "Time\n";
	private Text time;
	private double elapsedTime;
	private int timeSec;
	private int timeMin;
	private GameMode gameMode;
	private WindBlowingManager windManager;
	private WindDirection windDirection;
	private MiniMap minimap;
	private ButtonSprite minimap_button;
	private ButtonSprite weaponSelection_button;
	private WeaponSelectionArea weaponSelection;
	
	private boolean game_started;
	
	private boolean debug_mode;
	private ArrayList<Explosion> explosionList = new ArrayList<Explosion>();
	

	@Override
	public void createScene() {
		debug_mode = false;
		game_started = false;
		windManager = new WindBlowingManager();
		// create and initialize the mainGameScene
		createPhysics();
		initMap();
		settingLayers();
		createPlayer();
		createPlayingGame();
		createHUD();
		
		initZoomListener();
		initCamera();
		
		attachChilds();
		
		this.registerUpdateHandler(new IUpdateHandler() {

	        @Override
	        public void reset() {         
	        }

	        @Override
	        public void onUpdate(float pSecondsElapsed) {
	        	if(game_started){
				   if(map.need_update()){
					   map.updateMidGround();
					   setMidground(map.updatedGroundBlocks());
					   debug(debug_mode);
				   }
				   Iterator<Body> i =  mPhysicsWorld.getBodies();
				   while(i.hasNext()){
						Body b = i.next();
						if(b.getType() == BodyType.DynamicBody && !b.getFixtureList().get(0).isSensor()){
							b.applyForceToCenter(new Vector2(windManager.getWindForce(), 0));
						}
				   }
				   if(perso.getY() < 0) {
	        		   perso.onDie();
	        		   game_started = false;
		           }
				   /*Debug.e("potato", "" + explosionList.size());
				   if(explosionList.size() > 0){
					   Explosion e = explosionList.get(0);
					   Debug.e("potato", "onupdate" + ((ExplosionData)(e.getUserData())).getId());
					   e.createBody(mPhysicsWorld);
					   midLayer.attachChild(e);
					   explosionList.clear();
				   }*/
		        }
	        }
	    });
		
		this.registerUpdateHandler(new TimerHandler(0.1f, true, new ITimerCallback() {

				@Override
		        public void onTimePassed(TimerHandler pTimerHandler) {
					if(game_started){
						if(minimap.isShown()){
							update_minimap();
						}
					}
	                pTimerHandler.reset();
		        }
		}));

		this.registerUpdateHandler(new TimerHandler(1f, true, new ITimerCallback() {

				@Override
		        public void onTimePassed(TimerHandler pTimerHandler) {
					if(game_started){
						
						switch(gameMode){
						case NORMAL:
							elapsedTime ++;
							break;
						case TIME_ATTACK:
							elapsedTime --;
							break;
						default:
							break;
						}
						
		        		timeMin = (int)(elapsedTime / 60);
		        		timeSec = (int)(elapsedTime % 60);
		        		if(timeMin < 10){
		        			if(timeSec < 10){
		        				time.setText(timeText+"0"+timeMin+":0"+timeSec);
		        			}
		        			else{
		        				time.setText(timeText+"0"+timeMin+":"+timeSec);
		        			}
		        		}
		        		else{
		        			if(timeSec < 10){
		        				time.setText(timeText+timeMin+":0"+timeSec);
		        			}
		        			else{
		        				time.setText(timeText+timeMin+":"+timeSec);
		        			}
		        		}
		        		if((int)elapsedTime == 0){
		        			unregisterUpdateHandler(pTimerHandler);
		        		}
					}
	                pTimerHandler.reset();
		        }
		}));
		
		this.registerUpdateHandler(new TimerHandler(5f, true, new ITimerCallback() {

			@Override
	        public void onTimePassed(TimerHandler pTimerHandler) {
				if(game_started){
					windManager.update_wind();
					windDirection = windManager.getWindDirection();
					switch(windDirection){
					case NONE:
						flag.setCurrentTileIndex(2);
						break;
					case SOFT_LEFT:
						flag.setCurrentTileIndex(1);
						break;
					case SOFT_RIGHT:
						flag.setCurrentTileIndex(3);
						break;
					case STRONG_LEFT:
						flag.setCurrentTileIndex(0);
						break;
					case STRONG_RIGHT:
						flag.setCurrentTileIndex(4);
						break;
					default:
						break;
					}
				}
                pTimerHandler.reset();
	        }
	}));

		camera.setCenterDirect(perso.getY(), perso.getY());
		//draw the scene in debug mode
		debug(debug_mode);
		
		game_started = true;
	}
	
	/**
	 * create layers of our scene
	 */
	private void settingLayers() {
		backLayer = new Entity();
		midLayer = new Entity();
	}

	/**
	 * @param b
	 * Draw bodies fixtures lines in our physics world if param is true
	 */
	private void debug(boolean b) {
		if(b){
			DebugRenderer debug = new DebugRenderer(mPhysicsWorld, vbom);
			debug.setZIndex(1000);
			this.attachChild(debug);
		}
	}

	/**
	 * create a SceneTouchListener and 
	 * a zoom/dezoom functionality by screen pinching
	 */
	private void initZoomListener() {
		this.setOnSceneTouchListener(this);
		pinchZoomDetector = new PinchZoomDetector(this);
		pinchZoomDetector.setEnabled(true);
	}

	/**
	 * set the camera to have boundaries and 
	 * to chase after the player
	 */
	private void initCamera() {
		this.MIN_ZOOM_FACTOR = camera.getWidth() / this.WIDTH ;
		camera.setBounds(0, 0, this.WIDTH, this.WIDTH);
		camera.setBoundsEnabled(true);
		camera.setChaseEntity(perso);
	}

	/**
	 * initialize the game map of the scene from the resources manager
	 * map data
	 * physics World
	 * background texture
	 * midground texture
	 * vertex object manager
	 * set the dimension of the scene to correspond with map dimension
	 */
	private void initMap() {
		map = this.resourcesManager.map;
		map.init(this.mPhysicsWorld);
		this.setDimension(map.dimension);
	}


	private void setDimension(Vector2 dimension) {
		this.WIDTH = dimension.x;
		this.HEIGHT = dimension.y;
	}

	
	/**
	 * temp function to create a character and his body
	 */
	private void createPlayer() {
		Random r = new Random();
		int s = r.nextInt(map.spawnPoints.size());
		Vector2 p = map.spawnPoints.get(s);
		perso = new Patate( p.x, p.y, vbom, mPhysicsWorld){
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY){
				
					switch(pSceneTouchEvent.getAction()){
					case TouchEvent.ACTION_UP:
						PlayerData p = (PlayerData)perso.getUserData();
						if(p.health() > 0){
							p.damage(75);
						}else{
							p.full_healing();
						}
						update_healthbar();
					case TouchEvent.ACTION_DOWN:
					case TouchEvent.ACTION_CANCEL:
					case TouchEvent.ACTION_MOVE:
					default:
						break;
					}
					return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
			}
		};
		this.registerTouchArea(perso);
		perso.setLayer(midLayer);
		perso.changeWeapon(WeaponType.GUN);
	}


	/**
	 * tmp function to create an analog controler to move the character
	 */
	private void createPlayingGame() {
		this.windDirection = WindDirection.NONE;
		this.gameMode = GameMode.NORMAL;
		switch(this.gameMode){
		case NORMAL:
			this.elapsedTime = 0;
			break;
		case TIME_ATTACK:
			this.elapsedTime = 1*60+30;
			break;
		default:
			break;
		}
	}


	private void attachChilds() {
		this.setBackground(map.getBackground());
		this.attachChild(backLayer);
		this.attachChild(midLayer);
		this.setMidground(map.getMidGround());

		midLayer.attachChild(perso);
	}

	private void setMidground(List<GroundBlock> list) {
		Iterator<GroundBlock> i = list.iterator();
		while (i.hasNext()) {
			GroundBlock s = i.next();
			midLayer.attachChild(s);
		}
	}

	private void createPhysics() {

		mPhysicsWorld = new FixedStepPhysicsWorld(30, 
				new Vector2(0f, -SensorManager.GRAVITY_EARTH * 9.81f), false, 8, 3);
		mPhysicsWorld.setContactListener(new GameContactListener(this));
		this.registerUpdateHandler(mPhysicsWorld);
	}

	private void createHUD() {
		gameHUD = new HUD();
		/*
		 * Left Controler
		 */
		leftAnalogOnScreenControl = new AnalogOnScreenControl(25, 25, camera,
				resourcesManager.control_Base_Region,
				resourcesManager.control_Button_Region, 0.1f, 200, resourcesManager.vbom,
				new IAnalogOnScreenControlListener() {
					@Override
					public void onControlChange(
							final BaseOnScreenControl pBaseOnScreenControl,
							final float pValueX, final float pValueY) {

						if (pValueX != 0) {
							final Vector2 move = Vector2Pool.obtain(pValueX * 10f, perso.getBody().getLinearVelocity().y);
							perso.getBody().setLinearVelocity(move);
						}
						if(((PlayerData)perso.getUserData()).isJumping())
							perso.startRun(false);
						if (pValueX != 0) {

							if(pValueX < 0 && perso.goRight()) {
								perso.changeDirection();
							}
							if(pValueX > 0 && perso.goLeft()) {
								perso.changeDirection();
							}
							if(!((PlayerData)perso.getUserData()).isJumping())
								perso.startRun(true);
							final Vector2 move = Vector2Pool.obtain(pValueX * 10f, perso.getBody().getLinearVelocity().y);
							perso.getBody().setLinearVelocity(move);
						}
						else {
							if(!((PlayerData)perso.getUserData()).isJumping())
								perso.startRun(false);
						}
					}

					@Override
					public void onControlClick(
							AnalogOnScreenControl pAnalogOnScreenControl) {
						// TODO Auto-generated method stub
						
					}

				});

		final Sprite leftControlBase = leftAnalogOnScreenControl.getControlBase();
		leftControlBase.setAlpha(0.7f);
		leftControlBase.setOffsetCenter(0, 0);
		
		/*
		 * Right Controler
		 */
		rightAnalogOnScreenControl = new AnalogOnScreenControl(camera.getWidth()-125, 50, camera,
				resourcesManager.control_Base_Region,
				resourcesManager.control_Button_Region, 0.1f, 200, resourcesManager.vbom,
				new IAnalogOnScreenControlListener() {
					@Override
					public void onControlChange(
							final BaseOnScreenControl pBaseOnScreenControl,
							final float pValueX, final float pValueY) {
					}

					@Override
					public void onControlClick(
							final AnalogOnScreenControl pAnalogOnScreenControl) {

					}
				});

		final Sprite rightControlBase = rightAnalogOnScreenControl.getControlBase();
		rightControlBase.setAlpha(0.5f);
		rightControlBase.setOffsetCenter(0, 0);
		rightAnalogOnScreenControl.setVisible(false);
		
		/*
		 * button bottom-right
		 */
		OnClickListener buttonB_OnclickListener = new OnClickListener() {
			float pX;
			float pY;
			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pX != pTouchAreaLocalX || pY != pTouchAreaLocalY){
					perso.jump();
					pX = pTouchAreaLocalX;
					pY = pTouchAreaLocalY;
				}
			}
		};
		
		buttonB = new ButtonAnimatedSprite(camera.getWidth() - 128, 48, resourcesManager.action_Button_Region, resourcesManager.vbom, buttonB_OnclickListener, "B", resourcesManager.HUD_Button_font);
		buttonB.setCurrentTileIndex(0);
		buttonB.setAlpha(0.8f);
		
		/*
		 * button top-right
		 */
		OnClickListener buttonA_OnclickListener = new OnClickListener() {

			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				perso.attack();
			}
		};
		
		buttonA = new ButtonAnimatedSprite(camera.getWidth() - 64, 112, resourcesManager.action_Button_Region, resourcesManager.vbom, buttonA_OnclickListener, "A", resourcesManager.HUD_Button_font);
		buttonA.setAlpha(0.8f);
		buttonA.setCurrentTileIndex(0);
		
		/*
		 * Player healthBar
		 */
		String healthText = ((PlayerData)perso.getUserData()).healthToText();
		float bar_width = 175f;
		float bar_height = 25f;
		float bar_posX = bar_width/2 + 10;
		float bar_posY = camera.getHeight() - (bar_height/2 + 10);
		healthBar = new HealthBar(bar_posX, bar_posY, bar_width, bar_height, resourcesManager.vbom, healthText, resourcesManager.HUD_healthBar_font);
		
		/*
		 * Score
		 */
		float score_posX =  camera.getWidth()/3;
		float score_posY = camera.getHeight() - 24;
		this.scoreText = ((PlayerData)perso.getUserData()).ScoreToText();
		Text score = new Text(score_posX, score_posY, resourcesManager.HUD_Score_font, scoreText, new TextOptions(HorizontalAlign.CENTER), resourcesManager.vbom);
		
		/*
		 * Time
		 */
		float time_posX =  camera.getWidth()/2;
		float time_posY = camera.getHeight() - 24;
		time = new Text(time_posX, time_posY, resourcesManager.HUD_Score_font, timeText+"00:00", new TextOptions(HorizontalAlign.CENTER), resourcesManager.vbom);
		
		/*
		 * Wind flag
		 */
		float flag_posX = camera.getWidth() - camera.getWidth()/3;
		float flag_posY = camera.getHeight() - 20;
		flag = new TiledSprite(flag_posX, flag_posY, resourcesManager.wind_flag_storm, this.vbom);
		
		/*
		 * button_setting
		 */
		float setting_posX = camera.getWidth() - 24;
		float setting_posY = camera.getHeight() - 24;
		buttonSetting = new ButtonSprite(setting_posX, setting_posY, resourcesManager.button_setting, resourcesManager.vbom);
		buttonSetting.setAlpha(0.8f);
		buttonSetting.setScale(0.5f);
		
		/*
		 * minimap et minimap_icon
		 */
		float minimap_scale = (camera.getWidth() / this.WIDTH) * 0.2f;
		float minimap_icon_X = camera.getWidth() - camera.getWidth()/6;
		float minimap_icon_Y = camera.getHeight() - 20;
		float minimap_height = camera.getHeight() * 0.15f;
		float minimap_width = camera.getWidth() * 0.2f;
		float minimap_posX = camera.getWidth() - (minimap_width + 53f);
		float minimap_posY = camera.getHeight() - (minimap_height + 38f);
		minimap = new MiniMap(minimap_posX, minimap_posY, minimap_width, minimap_height, this.resourcesManager, minimap_scale);
		OnClickListener minimap_OnclickListener = new OnClickListener() {
			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				minimap.changeState();
			}
		};
		minimap_button = new ButtonSprite(minimap_icon_X, minimap_icon_Y, resourcesManager.minimap_icon, resourcesManager.vbom, minimap_OnclickListener);
		
		/*
		 * Weapon selection
		 */
		float weaponSelection_icon_X = camera.getWidth() - 32;
		float weaponSelection_icon_Y = 32;
		
		weaponSelection = new WeaponSelectionArea()
		{
			public float lastXpos = 0;
			public boolean isSlided = true;
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY){
				
					switch(pSceneTouchEvent.getAction()){
					case TouchEvent.ACTION_UP:
					case TouchEvent.ACTION_DOWN:
					case TouchEvent.ACTION_CANCEL:
						if (isSlided && lastXpos != 0)
						{
							if ((pTouchAreaLocalX-lastXpos)/96 > 0.3)
							{
								liste_sprite_armes.PermutePrev();
								weaponSelection.refresh();
								isSlided = false;
								lastXpos = 0;
							}
							else if ((pTouchAreaLocalX-lastXpos)/96 < -0.3)
							{
								liste_sprite_armes.PermuteNext();
								weaponSelection.refresh();
								isSlided = false;
								lastXpos = 0;
							}
							else
							{
								if (lastXpos > 250)
								{
									liste_sprite_armes.PermuteNext();
									weaponSelection.refresh();
								}
								else if (lastXpos < 120)
								{
									liste_sprite_armes.PermutePrev();
									weaponSelection.refresh();
								}
								isSlided = false;
								lastXpos = 0;
							}
						}
						else
						{
							lastXpos = pTouchAreaLocalX;
							isSlided = true;
						}
					case TouchEvent.ACTION_MOVE:
					default:
						break;
					}
					return true;
			}
		};
		weaponSelection.changeState();
		
		OnClickListener weaponSelection_OnclickListener = new OnClickListener() {
			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				weaponSelection.changeState();
				if(!weaponSelection.isVisible()){
					gameHUD.unregisterTouchArea(weaponSelection);
				}else{
					gameHUD.registerTouchArea(weaponSelection);
				}
			}
		};
		
		weaponSelection_button = new ButtonSprite(weaponSelection_icon_X, weaponSelection_icon_Y, resourcesManager.weapon_selection_icon, resourcesManager.vbom, weaponSelection_OnclickListener);
		weaponSelection_button.setScale(2.f);
		
		gameHUD.registerTouchArea(buttonA);
		gameHUD.registerTouchArea(buttonB);
		gameHUD.registerTouchArea(buttonSetting);
		gameHUD.registerTouchArea(minimap_button);
		gameHUD.registerTouchArea(weaponSelection_button);
		gameHUD.registerTouchArea(weaponSelection.button_next);
		gameHUD.registerTouchArea(weaponSelection.button_prev);
		gameHUD.attachChild(buttonA);
		gameHUD.attachChild(buttonB);
		gameHUD.attachChild(healthBar);
		gameHUD.attachChild(score);
		gameHUD.attachChild(time);
		gameHUD.attachChild(flag);
		//gameHUD.attachChild(buttonSetting);
		gameHUD.attachChild(minimap_button);
		gameHUD.attachChild(weaponSelection_button);
		gameHUD.attachChild(minimap);
		gameHUD.attachChild(weaponSelection);
		gameHUD.setChildScene(leftAnalogOnScreenControl);
		//leftAnalogOnScreenControl.setChildScene(rightAnalogOnScreenControl);
		camera.setHUD(gameHUD);
	}
	
	public void update_minimap(){
		minimap.setGround(this.map.getMidGround());
		minimap.setPlayer(new Vector2(this.perso.getX() + this.perso.getWidth()/2, this.perso.getY()));
		minimap.update();
	}
	
	public void update_healthbar(){
		PlayerData p = (PlayerData)perso.getUserData();
		healthBar.update(p.getState(), p.health_percent(), p.healthToText());
		if(!p.isAlive()) {
			perso.onDie();
			gameHUD.detachChild(healthBar);			
			gameHUD.detachChild(buttonA);
			gameHUD.unregisterTouchArea(buttonA);
			gameHUD.detachChild(buttonB);
			gameHUD.unregisterTouchArea(buttonB);
			leftAnalogOnScreenControl.setVisible(false);
			gameHUD.unregisterTouchArea(leftAnalogOnScreenControl);
		}
		
	}
	
	public void addExplosion(Explosion e){
		this.explosionList.add(e);
	}
	
	/**
	 * (non-Javadoc)
	 * @see org.andengine.input.touch.detector.PinchZoomDetector.IPinchZoomDetectorListener#onPinchZoomStarted(org.andengine.input.touch.detector.PinchZoomDetector, org.andengine.input.touch.TouchEvent)
	 */
	@Override
	public void onPinchZoomStarted(PinchZoomDetector pPinchZoomDetector,
			TouchEvent pSceneTouchEvent) {
		this.initialTouchZoomFactor = camera.getZoomFactor();
	}

	/**
	 * (non-Javadoc)
	 * @see org.andengine.input.touch.detector.PinchZoomDetector.IPinchZoomDetectorListener#onPinchZoom(org.andengine.input.touch.detector.PinchZoomDetector, org.andengine.input.touch.TouchEvent, float)
	 */
	@Override
	public void onPinchZoom(PinchZoomDetector pPinchZoomDetector,
			TouchEvent pTouchEvent, float pZoomFactor) {
		final float newZoomFactor = this.initialTouchZoomFactor * pZoomFactor;
		if (newZoomFactor < MAX_ZOOM_FACTOR && newZoomFactor > MIN_ZOOM_FACTOR) {
			camera.setZoomFactor(newZoomFactor);
		}

	}

	/**
	 * (non-Javadoc)
	 * @see org.andengine.input.touch.detector.PinchZoomDetector.IPinchZoomDetectorListener#onPinchZoomFinished(org.andengine.input.touch.detector.PinchZoomDetector, org.andengine.input.touch.TouchEvent, float)
	 */
	@Override
	public void onPinchZoomFinished(PinchZoomDetector pPinchZoomDetector,
			TouchEvent pTouchEvent, float pZoomFactor) {
		final float newZoomFactor = this.initialTouchZoomFactor * pZoomFactor;
		if (newZoomFactor < MAX_ZOOM_FACTOR && newZoomFactor > MIN_ZOOM_FACTOR) {
			camera.setZoomFactor(newZoomFactor);
		}

	}

	/**
	 * (non-Javadoc)
	 * @see org.andengine.entity.scene.IOnSceneTouchListener#onSceneTouchEvent(org.andengine.entity.scene.Scene, org.andengine.input.touch.TouchEvent)
	 */
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if(pSceneTouchEvent.isActionUp()){
			final Explosion explosion = new Explosion(pSceneTouchEvent.getX(), pSceneTouchEvent.getY(), 100f);
			explosion.createBody(mPhysicsWorld);
			midLayer.attachChild(explosion);
		}
		
		this.pinchZoomDetector.onTouchEvent(pSceneTouchEvent);
		this.sortChildren(false);
		return true;
	}
	
	/**
	 * (non-Javadoc)
	 * @see com.potatoe_team.potatoe_fight.scene.BaseScene#onBackKeyPressed()
	 */
	@Override
	public void onBackKeyPressed() {
		SceneManager.getInstance().backToMenuScene();
		SceneManager.getInstance().disposeGameScene();
	}

	/**
	 * (non-Javadoc)
	 * @see com.potatoe_team.potatoe_fight.scene.BaseScene#getSceneType()
	 */
	@Override
	public SceneType getSceneType() {

		return SceneType.SCENE_GAME;
	}

	/**
	 * (non-Javadoc)
	 * @see com.potatoe_team.potatoe_fight.scene.BaseScene#disposeScene()
	 */
	@Override
	public void disposeScene() {
		this.gameHUD.clearChildScene();
		this.gameHUD.detachChildren();
		this.detachChildren();
		this.detachSelf();
		this.dispose();
	}
	
	
}
