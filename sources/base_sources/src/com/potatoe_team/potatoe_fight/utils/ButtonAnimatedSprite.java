package com.potatoe_team.potatoe_fight.utils;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
 
//this is a class that animates a menu item button when pressed by using a tiled texture with two images (normal and pressed). 
//it also displays the supplied text on the button
public class ButtonAnimatedSprite extends ButtonSprite {
 private float textX = 0;
 private float textY = 0;
 private Text buttonText;
  
  
 public ButtonAnimatedSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
  super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
 }
  
 public ButtonAnimatedSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, OnClickListener pOnClickListener) {
  super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOnClickListener);
 }
  
 public ButtonAnimatedSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, String pText, Font pFont) {
	 this(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, new Text(0, 0, pFont, pText, pVertexBufferObjectManager));
 }
  
 public ButtonAnimatedSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, OnClickListener pOnClickListener, String pText, Font pFont) {
	 this(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOnClickListener, new Text(0, 0, pFont, pText, pVertexBufferObjectManager));
 }
 
 public ButtonAnimatedSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, Text pText) {
	  super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
	  buttonText = pText;
	  textX = (this.getWidth() - this.getWidth()/2);
	  textY = (this.getHeight() - this.getHeight()/2);
	  buttonText.setPosition(textX, textY);
	  this.attachChild(buttonText);
	 }
	  
	 public ButtonAnimatedSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, OnClickListener pOnClickListener, Text pText) {
	  super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOnClickListener);
	  buttonText = pText;
	  textX = (this.getWidth() - this.getWidth()/2);
	  textY = (this.getHeight() - this.getHeight()/2);
	  buttonText.setPosition(textX, textY);
	  this.attachChild(buttonText);
	 }
  
 
 public void pressButton() {
  this.setCurrentTileIndex(1);
 }
 
 public void depressButton() {
  setCurrentTileIndex(0);
 }
 
}
