package com.potatoe_team.potatoe_fight.utils;

import java.util.ArrayList;

import org.andengine.entity.sprite.Sprite;

public class WeaponList extends ArrayList<Sprite> {

	private static final long serialVersionUID = 1L;
	
	
	WeaponList ()
	{
		super ();
	}
	
	public void PermuteNext ()
	{
		if (size() > 0)
		{
			Sprite tmp = get(0);
			for (int i = 0; i < size()-1; i++)
				set(i, get(i+1));
			set(size()-1, tmp);
		}
	}
	
	public void PermutePrev ()
	{
		if (size () > 0)
		{
			Sprite tmp = get(size()-1);
			for (int i = size()-1; i > 0; i--)
				set(i, get(i-1));
			set(0, tmp);
		}
	}
	
}
