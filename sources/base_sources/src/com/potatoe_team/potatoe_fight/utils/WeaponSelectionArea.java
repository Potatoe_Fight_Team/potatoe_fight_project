package com.potatoe_team.potatoe_fight.utils;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;

import com.potatoe_team.potatoe_fight.manager.ResourcesManager;

public class WeaponSelectionArea extends Rectangle 
{
	public ButtonSprite button_next;
	public ButtonSprite button_prev;
	public Sprite cadre_current_weapon;
	public WeaponList liste_sprite_armes;

	public WeaponSelectionArea() {
		super(400, 70, 400, 120, ResourcesManager.getInstance().vbom);
		this.setAlpha(0);
		liste_sprite_armes = new WeaponList();
		liste_sprite_armes.add(new Sprite(86,60,96, 96, ResourcesManager.getInstance().arme1,  ResourcesManager.getInstance().vbom));
		liste_sprite_armes.add(new Sprite(200,60,96, 96, ResourcesManager.getInstance().arme2,  ResourcesManager.getInstance().vbom));
		liste_sprite_armes.add(new Sprite(314,60,96, 96, ResourcesManager.getInstance().arme3,  ResourcesManager.getInstance().vbom));
		button_prev = new ButtonSprite(16, 60, ResourcesManager.getInstance().weapon_selection_left, ResourcesManager.getInstance().vbom,
			new OnClickListener()
			{
				@Override
				public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY)
				{
					liste_sprite_armes.PermutePrev();
					refresh();
				}
			}
		);	
		button_next = new ButtonSprite(384, 60, ResourcesManager.getInstance().weapon_selection_right, ResourcesManager.getInstance().vbom, 
			new OnClickListener()
			{
				@Override
				public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY)
				{
					liste_sprite_armes.PermuteNext();
					refresh();
				}
			}
		);
		cadre_current_weapon = new Sprite (200, 60, 120, 120, ResourcesManager.getInstance().weapon_selection_frame, ResourcesManager.getInstance().vbom);
		for (Sprite s : liste_sprite_armes) {
			this.attachChild(s);
		}
		this.attachChild(cadre_current_weapon);
		this.attachChild(button_prev);
		this.attachChild(button_next);
		}
	public void refresh()
	{
		for (Sprite s : liste_sprite_armes) {
			this.detachChild(s);
		}
		for (int i = 0; i < 3; i++) {
			if (i == 0)
			{
				liste_sprite_armes.get(i).setX(86);
				//liste_sprite_armes.get(i).setAnchorCenterX(86);
				//liste_sprite_armes.get(i).setAnchorCenter(86, 60);
			}
			else if (i == 1)
			{
				liste_sprite_armes.get(i).setX(200);
				//liste_sprite_armes.get(i).setAnchorCenter(200, 60);
			}
			else
			{
				liste_sprite_armes.get(i).setX(314);
				//liste_sprite_armes.get(i).setAnchorCenter(314, 60);
			}
			this.attachChild(liste_sprite_armes.get(i));
		}
		//this.attachChild(cadre_current_weapon);
	}
	public void changeState() {
		this.setVisible(!this.isVisible());
	}
}

