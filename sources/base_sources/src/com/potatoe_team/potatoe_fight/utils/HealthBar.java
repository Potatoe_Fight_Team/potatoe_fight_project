/**
 * 
 */
package com.potatoe_team.potatoe_fight.utils;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;

import com.potatoe_team.potatoe_fight.user_data.PlayerData.HealthState;

/**
 * @author billy
 *
 */
public class HealthBar extends Rectangle{

	private Text text;
	private Rectangle indicator;
	
	public HealthBar(float pX, float pY, float pWidth, float pHeight, 
					  VertexBufferObjectManager vbom, String pText, Font pFont){
		super(pX, pY, pWidth, pHeight, vbom);
		this.setColor(Color.WHITE);
		this.setAlpha(0.3f);
		indicator = new Rectangle(0, 0, pWidth, pHeight, vbom);
		indicator.setAnchorCenter(0, 0);
		pFont.prepareLetters("0123456789/%".toCharArray());
		this.text = new Text(this.getWidth()/2, this.getHeight()/2, pFont, pText, new TextOptions(HorizontalAlign.CENTER), vbom);
		this.attachChild(indicator);
		this.attachChild(text);
		this.update(HealthState.GOOD, 100, pText);
	}
	
	public void update(HealthState healthState, int health_percent, String ptext){
		indicator.setWidth(this.getWidth()*health_percent/100);
		this.selectMode(healthState);
		this.updateText(ptext);
	}

	private void updateText(String pText) {
		this.text.setText(pText);
	}
	
	public void changeText(String pText){
		this.updateText(pText);
	}

	private void selectMode(HealthState s) {
		float alpha_value = 0.8f;
		switch(s){
			case GOOD:
				indicator.setColor(Color.GREEN);
				indicator.setAlpha(alpha_value);
				break;
			case AVERAGE:
				indicator.setColor(Color.YELLOW);
				indicator.setAlpha(alpha_value);
				break;
			case BAD:
				indicator.setColor(1, 0.64f, 0); //orange
				indicator.setAlpha(alpha_value);
				break;
			case REALLY_BAD:
				indicator.setColor(Color.RED);
				indicator.setAlpha(alpha_value);
				break;
			case DEAD:
				indicator.setColor(Color.BLACK);
				indicator.setAlpha(alpha_value);
				break;
		}
	}

}
