package com.potatoe_team.potatoe_fight.utils;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author billy
 *
 */
public class PositionIndicator extends Sprite {

	private static final Vector2 indicator_dim = new Vector2(12,12);
	
	public PositionIndicator(Vector2 pPos, ITextureRegion pTextureRegion, VertexBufferObjectManager pVbom){
		this(pPos.x, pPos.y, pTextureRegion, pVbom);
	}
	
	public PositionIndicator(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVbom) {
		super(pX, pY, indicator_dim.x, indicator_dim.y, pTextureRegion, pVbom);
	}
	
}
