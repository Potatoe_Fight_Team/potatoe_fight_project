/**
 * 
 */
package com.potatoe_team.potatoe_fight.utils.geom2D;

import com.badlogic.gdx.math.Vector2;

/**
 * @author billy
 *
 */
public class Circle {

	private Vector2 center;
	private float radius;
	
	/**
	 * 
	 * @param pCenter
	 * @param pRadius
	 */
	public Circle(Vector2 pCenter, float pRadius) {
		this.center = pCenter;
		this.radius = pRadius;
	}

	/**
	 * 
	 * @return
	 */
	public Vector2 center(){
		return this.center;
	}
	
	/**
	 * 
	 * @return
	 */
	public float radius(){
		return this.radius;
	}
	
	/**
	 * 
	 * @param absciss
	 * @return
	 */
	public float ordinate(float absciss){
		return (float)(Math.sqrt((this.radius * this.radius) - (absciss * absciss)));
	}
	
	/**
	 * 
	 * @param ordinate
	 * @return
	 */
	public float absciss(float ordinate){
		return (float)(Math.sqrt((this.radius * this.radius) - (ordinate * ordinate)));
	}
	
	/**
	 * 
	 * @param absciss
	 * @return
	 */
	public float ordinate_angle(float angle){
		return this.radius*(float)(Math.sin(angle));
	}
	
	/**
	 * 
	 * @param ordinate
	 * @return
	 */
	public float absciss_angle(float angle){
		return this.radius*(float)(Math.cos(angle));
	}
	
	/**
	 * Check if a point is on the circle
	 * @param p
	 * @return
	 */
	public boolean intersection(Vector2 p){
		float d = this.center.dst(p) - this.radius;
		d *= d;
		float eps = 0.5f;
		return (d <= eps);
	}
	
	/**
	 * check if a point is in the circle
	 * @param p
	 * @return
	 */
	public boolean contains(Vector2 p){
		float eps = 0.01f;
		float d = this.radius - this.center.dst(p);
		return (d >= (0-eps));
	}
	
}
