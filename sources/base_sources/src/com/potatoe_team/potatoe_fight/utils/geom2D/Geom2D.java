/**
 * 
 */
package com.potatoe_team.potatoe_fight.utils.geom2D;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.util.GeometricShapeFactory;

/**
 * @author billy
 *
 */
public class Geom2D {

	
	/**
	 * 
	 * @param center
	 * @param radius
	 * @return
	 */
	
	public static Circle createCircle(Vector2 center, float radius){
		return new Circle(center, radius);
	}
	
	
	/**
	 * 
	 * @param p
	 * @return
	 */
	public static Coordinate coordFromVector2(Vector2 p){
		return new Coordinate(p.x, p.y);
	}
	
	public static Vector2 vector2FromCoord(Coordinate c){
		return new Vector2((float)c.x, (float)c.y);
	}
	
	/**
	 * 
	 * @param c
	 * @param n
	 * @return
	 */
	public static Polygon createPolyCircle(Circle c, int n) {
		  GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
		  shapeFactory.setNumPoints(n);
		  shapeFactory.setCentre(coordFromVector2(c.center()));
		  shapeFactory.setSize(c.radius() * 2);
		  return shapeFactory.createCircle();
	}
	
	/**
	 * 
	 * @param l
	 * @return
	 */
	public static Polygon createPolygon(ArrayList<Vector2> l){
		  GeometryFactory geomFactory = new GeometryFactory();
		  Coordinate[] coordinates = new Coordinate[l.size()+1];
		  for(int i=0; i < l.size()+1; i++){
			  coordinates[i] = coordFromVector2(l.get(i%l.size()));
		  }
		  return geomFactory.createPolygon(geomFactory.createLinearRing(coordinates), null);
	}
	
}
