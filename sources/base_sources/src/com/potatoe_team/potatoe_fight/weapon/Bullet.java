package com.potatoe_team.potatoe_fight.weapon;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.user_data.ProjectileData;

public class Bullet extends Sprite{
	private Body mBody;
	private  ProjectileData mData;
	private int degat;
	private FixedStepPhysicsWorld mPhysicsWorld;
	public Bullet(float pX, float pY, VertexBufferObjectManager vbom, BoundCamera camera,FixedStepPhysicsWorld pPhysicsWorld, int degat, int direction){
		super(pX, pY,ResourcesManager.getInstance().bullet_region, vbom);
		mData = new ProjectileData(this);
		mData.setPiercing_potential(degat);
		mData.setVelocity(90 * direction);
		mPhysicsWorld = pPhysicsWorld;
		this.setScale(0.15f);
		mBody = PhysicsFactory.createBoxBody(mPhysicsWorld, this, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 1.0f, false));
		mBody.setBullet(true);
		mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(this, mBody, true, true));
		mBody.setGravityScale(0);
		mBody.setUserData(mData);
		mBody.setLinearVelocity(new Vector2(mData.getVelocity(), 0));
		
	}
	
	public Body getBody() {
		return mBody;
	}
	public void setBody(Body mBody) {
		this.mBody = mBody;
	}

	public ProjectileData getData() {
		return mData;
	}

	public void setData(ProjectileData mData) {
		this.mData = mData;
	}

	public int getDegat() {
		return degat;
	}


	public void setDegat(int degat) {
		this.degat = degat;
	}
	public void destroy() {
		
		final Sprite shape = this;
		
		ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
				mPhysicsWorld.unregisterPhysicsConnector(mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(shape));
				if(mBody!=null){
					mPhysicsWorld.destroyBody(mBody);
					mBody = null;				
					shape.detachSelf();
					shape.dispose();
				}
			}
		});	
	}
	
//	public void setDegat(int degat) {
//		// TODO Auto-generated method stub
//		this.degat=degat;
//	}
}
