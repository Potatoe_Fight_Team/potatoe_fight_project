package com.potatoe_team.potatoe_fight.weapon;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public abstract class ImpactWeapon extends FirearmWeapon {

	public ImpactWeapon(float pX, float pY, VertexBufferObjectManager vbom,
			BoundCamera camera, ITiledTextureRegion arme_region, int dammage,
			int maxAmmo) {
		super(pX, pY, vbom, camera, arme_region, dammage, maxAmmo);
		// TODO Auto-generated constructor stub
	}
	public ImpactWeapon(float pX, float pY, VertexBufferObjectManager vbom,
			BoundCamera camera, ITiledTextureRegion arme_region, int dammage) {
		super(pX, pY, vbom, camera, arme_region, dammage);
		// TODO Auto-generated constructor stub
	}
	@Override
	public abstract void shoot();
}
