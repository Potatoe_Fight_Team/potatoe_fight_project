/**
 * 
 */
package com.potatoe_team.potatoe_fight.weapon;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsFactory;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.user_data.ExplosionData;

/**
 * @author billy
 *
 */
public class Explosion extends AnimatedSprite {
	
	private Body mBody;
	private ExplosionData mData;
	private final long[] frameDuration = {50, 50, 50};
	private FixedStepPhysicsWorld mPhysicsWorld;
	
	public Explosion(float pX, float pY, float pRadius){
		super(pX, pY, 2*pRadius, 2*pRadius, ResourcesManager.getInstance().explosion_Region, ResourcesManager.getInstance().vbom);
		this.animate(frameDuration, false, new IAnimationListener(){

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
					int pInitialLoopCount) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
					int pOldFrameIndex, int pNewFrameIndex) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
					int pRemainingLoopCount, int pInitialLoopCount) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationFinished(final AnimatedSprite pAnimatedSprite) {
				ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){

					@Override
					public void run() {
						if(mPhysicsWorld != null && mBody != null){
							mPhysicsWorld.unregisterPhysicsConnector(mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(pAnimatedSprite));
							mBody.setActive(false);
							mPhysicsWorld.destroyBody(mBody);
							mBody = null;
						}
						pAnimatedSprite.detachSelf();
						pAnimatedSprite.dispose();
					}
					
				});
				
			}
			
		});
		mData = new ExplosionData(this);
		this.setUserData(mData);
	}
	
	
	public Body createBody(FixedStepPhysicsWorld pPhysicsWorld){
		mPhysicsWorld = pPhysicsWorld;
		mBody = PhysicsFactory.createCircleBody(mPhysicsWorld, this, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 1.0f, true));
		mBody.setGravityScale(0);
		mBody.setUserData(this.mData);
		this.mData.setCenter(new Vector2(this.getX(), this.getY()));
		this.mData.setRadius(this.getWidth()/2);
		return mBody;
	}
	
	public void destroyBody(){
		final AnimatedSprite shape = this;
		ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){

			@Override
			public void run() {
				if(mPhysicsWorld != null && mBody != null){
					mPhysicsWorld.unregisterPhysicsConnector(mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(shape));
					mBody.setActive(false);
					mPhysicsWorld.destroyBody(mBody);
					mBody = null;
				}
			}
			
		});
	}
	
}
