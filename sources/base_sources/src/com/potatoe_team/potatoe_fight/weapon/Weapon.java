package com.potatoe_team.potatoe_fight.weapon;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.player.Patate;
import com.potatoe_team.potatoe_fight.user_data.ObjectData.UserDataType;
import com.potatoe_team.potatoe_fight.weapon.WeaponList.WeaponType;

public abstract class Weapon extends AnimatedSprite {
	
	//public int remainingAmmo;
	//public int maxAmmo;
	public int dammage;
	//public boolean enable;
	//public ITiledTextureRegion arme;
	public Body mBody;
	
	public WeaponType type;
	
	public Weapon(float pX, float pY, VertexBufferObjectManager vbom, BoundCamera camera, ITiledTextureRegion arme_region,int dammage) {
        super(pX, pY,arme_region, vbom);
        this.dammage=dammage;
    }
	public Weapon(float pX, float pY, VertexBufferObjectManager vbom, BoundCamera camera, ITiledTextureRegion arme_region) {
        this(pX, pY, vbom, camera, arme_region, 0);
    }

	public int getDegat() {
		return dammage;
	}

	public void setDegat(int dammage) {
		this.dammage = dammage;
	}
	
	public Body createBody(FixedStepPhysicsWorld pPhysicsWorld){
			mBody = PhysicsFactory.createBoxBody(pPhysicsWorld, this, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 1.0f,true));
			mBody.setGravityScale(0);
			return mBody;
	}
	
	public Body getBody() {
		return mBody;
	}
	
	public void setBody(Body mBody) {
		this.mBody = mBody;
	}
	
	
}