package com.potatoe_team.potatoe_fight.weapon;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public abstract class RangedWeapon extends Weapon{
	public int maxAmmo;
	public int remainingAmmo;
	public boolean enable;
	
	public RangedWeapon(float pX, float pY, VertexBufferObjectManager vbom,
			BoundCamera camera, ITiledTextureRegion arme_region,int dammage,int maxAmmo) {
		super(pX, pY, vbom, camera, arme_region,dammage);
		this.maxAmmo=maxAmmo;
	}
	public RangedWeapon(float pX, float pY, VertexBufferObjectManager vbom,
			BoundCamera camera, ITiledTextureRegion arme_region,int dammage) {
		super(pX, pY, vbom, camera, arme_region,dammage);
	}

	public int getMaxAmmo() {
		return maxAmmo;
	}

	public int getRemainingAmmo() {
		return remainingAmmo;
	}

	public void setRemainingAmmo(int remainingAmmo) {
		this.remainingAmmo = remainingAmmo;
	}

	public boolean checkEnable() {
		if(remainingAmmo>0) enable=true;
		else enable=false;
		return enable;
	}
	
	public abstract void shoot();
}
