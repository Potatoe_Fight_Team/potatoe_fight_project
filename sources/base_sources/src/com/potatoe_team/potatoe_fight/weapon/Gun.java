/**
 * 
 */
package com.potatoe_team.potatoe_fight.weapon;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.potatoe_team.potatoe_fight.weapon.WeaponList.WeaponType;

/**
 * @author liuxiaoyi
 *
 */
public class Gun extends ImpactWeapon {
	/**
	 * 
	 * @param pX
	 * @param pY
	 * @param vbom
	 * @param camera
	 * @param arme_region
	 *  constructor maxiAmmo 12, degat 2
	 */
	public Gun(float pX, float pY, VertexBufferObjectManager vbom,
			BoundCamera camera, ITiledTextureRegion arme_region) {
		super(pX, pY, vbom, camera, arme_region,2,12);
		this.type = WeaponType.GUN;
		this.setScale(0.4f);	
	}

	@Override
	public void shoot() {
		if(checkEnable()==true) this.remainingAmmo--;
	}

	
	
}
