package com.potatoe_team.potatoe_fight.weapon;

import com.potatoe_team.potatoe_fight.user_data.ObjectData.UserDataType;

public class WeaponList {

	public static enum WeaponType {
		GUN(0), KNIFE(1), ROCKET(2), GRENADE(3);
		
		public static WeaponType[] valueTypes = new WeaponType[] {GUN, KNIFE, ROCKET, GRENADE};
		private int value;
		
		WeaponType (int value) {
			this.value = value;
		}

		public int getValue () {
			return value;
		}	
	}

}
