package com.potatoe_team.potatoe_fight.physic;


import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.Vector2Pool;
import org.andengine.input.sensor.acceleration.AccelerationData;
import org.andengine.input.sensor.acceleration.IAccelerationListener;
import org.andengine.input.touch.TouchEvent;

import android.hardware.SensorManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.potatoe_team.potatoe_fight.MainActivity;

public class Physic extends MainActivity implements IAccelerationListener, IOnSceneTouchListener {
	
	public static int WIDTH = 800;
	public static int HEIGHT = 480;
	
	public Scene mScene;
	
	public FixedStepPhysicsWorld mPhysicsWorld;
	public Body groundWallBody;
	public Body roofWallBody;
	public Body leftWallBody;
	public Body rightWallBody;
	
	public Physic(Scene pScene) {
		
		this.mScene = pScene;
	}
	
	/**
	 * The onPopulateScene method was introduced to AndEngine as a way of
	 * separating scene-creation from scene population. This method is in place
	 * for attaching child entities to the scene once it has already been
	 * returned to the engine and set as our main scene.
	 * 
	 * @param pScene The Scene to populate.
	 * @param pOnPopulateSceneCallback Method use to callback at the end of populating scene.
	 */
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) {
		
		mPhysicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0f,-SensorManager.GRAVITY_EARTH*2), false, 8, 3);
		mScene.registerUpdateHandler(mPhysicsWorld);
		final FixtureDef WALL_FIXTURE_DEF = PhysicsFactory.createFixtureDef(0, 0.1f, 0.5f);
		
		final Rectangle ground = new Rectangle(WIDTH / 2f, 6f, WIDTH - 4f, 8f, this.getVertexBufferObjectManager());
		final Rectangle roof = new Rectangle(WIDTH / 2f, HEIGHT - 6f, WIDTH - 4f, 8f, this.getVertexBufferObjectManager());
		final Rectangle left = new Rectangle(6f, HEIGHT / 2f, 8f, HEIGHT - 4f, this.getVertexBufferObjectManager());
		final Rectangle right = new Rectangle(WIDTH - 6f, HEIGHT / 2f, 8f, HEIGHT - 4f, this.getVertexBufferObjectManager());
		
		groundWallBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, ground, BodyType.StaticBody, WALL_FIXTURE_DEF);
		roofWallBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, roof, BodyType.StaticBody, WALL_FIXTURE_DEF);
		leftWallBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, left, BodyType.StaticBody, WALL_FIXTURE_DEF);
		rightWallBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, right, BodyType.StaticBody, WALL_FIXTURE_DEF);
				
		this.mScene.attachChild(ground);
		this.mScene.attachChild(roof);
		this.mScene.attachChild(left);
		this.mScene.attachChild(right);
		
		mScene.setOnSceneTouchListener(this);
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	@Override
	public void onAccelerationAccuracyChanged(AccelerationData pAccelerationData) {
		
	}

	@Override
	public void onAccelerationChanged(AccelerationData pAccelerationData) {
		
		final Vector2 gravity = Vector2Pool.obtain(pAccelerationData.getX(), pAccelerationData.getY());
		this.mPhysicsWorld.setGravity(gravity);
		Vector2Pool.recycle(gravity);
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		
		return true;
	}
}
