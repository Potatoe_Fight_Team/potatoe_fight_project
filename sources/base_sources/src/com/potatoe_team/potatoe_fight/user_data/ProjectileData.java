/**
 * 
 */
package com.potatoe_team.potatoe_fight.user_data;

/**
 * @author billy
 *
 */
public class ProjectileData extends ObjectData {
	
	/**
	 * Projectile potential of penetration float value between 0 and 1.
	 * 0 -> can't pierce anything
	 * 0.5 and below -> small impact (length of projectile)
	 * above to make it pierce more deeply
	 * 1 -> can pierce through everything
	 */
	private float piercingPotential = 1.f;
	
	private float velocity;
	
	public ProjectileData(Object parent){
		super(UserDataType.PROJECTILE, parent);
	}
	
	public ProjectileData(String pName, Object parent){
		super(pName, UserDataType.PROJECTILE, parent);
	}
	
	/**
	 * @return the piercing_potential
	 */
	public float getPiercing_potential() {
		return piercingPotential;
	}

	/**
	 * @param piercing_potential the piercing_potential to set
	 */
	public void setPiercing_potential(float pPiercingPotential) {
		this.piercingPotential = pPiercingPotential;
	}

	/**
	 * @return the velocity
	 */
	public float getVelocity() {
		return velocity;
	}

	/**
	 * @param velocity the velocity to set
	 */
	public void setVelocity(float pVelocity) {
		this.velocity = pVelocity;
	}
	
	/**
	 * 
	 * @return the piercing value calculated from the velocity at impact and the piercing potential
	 */
	public float impact(){
		float piercingValue = this.velocity * this.piercingPotential;
		//change velocity of bullet after impact
		//if piercingPotential = 1, the velocity stay the same otherwise, it decrease until it fall to 0
		this.velocity = Math.max(2 * piercingValue - this.velocity, 0.0f);
		return piercingValue;
	}
	
}
