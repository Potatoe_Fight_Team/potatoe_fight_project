/**
 * 
 */
package com.potatoe_team.potatoe_fight.user_data;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;


/**
 * @author billy
 *
 */
public class GroundData extends ObjectData {

	private ArrayList<Vector2> vertices;

	public GroundData(Object parent){
		super(UserDataType.GROUND, parent);
	}
	
	public GroundData(String pName, Object parent){
		super(pName, UserDataType.GROUND, parent);
	}

	public void setVertices(ArrayList<Vector2> pVertices) {
		this.vertices = pVertices;
	}
	
	public ArrayList<Vector2> getVertices(){
		return this.vertices;
	}
	
}
