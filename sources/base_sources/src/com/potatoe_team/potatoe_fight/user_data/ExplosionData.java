/**
 * 
 */
package com.potatoe_team.potatoe_fight.user_data;

import com.badlogic.gdx.math.Vector2;

/**
 * @author billy
 *
 */
public class ExplosionData extends ObjectData {
	
	private Vector2 center;
	private float radius;
	private float blast_power;
	
	/**
	 * 
	 * @param parent
	 */
	public ExplosionData(Object parent){
		super(UserDataType.EXPLOSION, parent);
	}

	/**
	 * 
	 * @param pName
	 * @param parent
	 */
	public ExplosionData(String pName, Object parent){
		super(pName, UserDataType.EXPLOSION, parent);
	}

	/**
	 * @return the center
	 */
	public Vector2 getCenter() {
		return this.center;
	}

	/**
	 * @param center the center to set
	 */
	public void setCenter(Vector2 pCenter) {
		this.center = pCenter;
	}

	/**
	 * @return the radius
	 */
	public float getRadius() {
		return this.radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(float pRadius) {
		this.radius = pRadius;
	}

	/**
	 * @return the blast_power
	 */
	public float getBlastPower() {
		return this.blast_power;
	}

	/**
	 * @param blast_power the blast_power to set
	 */
	public void setBlastPower(float pBlast_power) {
		this.blast_power = pBlast_power;
	}
	
}
