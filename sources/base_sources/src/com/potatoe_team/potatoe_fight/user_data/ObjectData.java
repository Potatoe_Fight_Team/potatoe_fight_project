/**
 * 
 */
package com.potatoe_team.potatoe_fight.user_data;


/**
 * @author billy
 *
 */

public abstract class ObjectData {

	public static enum UserDataType {
		UNKNOWN(0), PLAYER(1), GROUND(2), PROJECTILE(3), EXPLOSIVE_PROJECTILE(4), EXPLOSION(5);
		
		public static UserDataType[] valueTypes = new UserDataType[] {UNKNOWN, PLAYER, GROUND, PROJECTILE, EXPLOSIVE_PROJECTILE, EXPLOSION};
		private int value;
		
		UserDataType (int value) {
			this.value = value;
		}

		public int getValue () {
			return value;
		}	
	}
	
	private static int Entity_id = 0;
	
	protected final int id;
	
	protected String name;
	
	private UserDataType dataType;
	
	private Object parent;
	
	public ObjectData(UserDataType pType, Object pParent){
		this.id = ObjectData.Entity_id;
		ObjectData.Entity_id ++;
		this.dataType = pType;
		this.name = "noname_" + this.id;
		this.parent = pParent;
	}

	/**
	 * 
	 * @param pName
	 */
	public ObjectData(String pName, UserDataType pType, Object pParent){
		this.id = ObjectData.Entity_id;
		ObjectData.Entity_id ++;
		this.dataType = pType;
		this.name = pName;
		this.parent = pParent;
	}
	
	/**
	 * 
	 * @param pName
	 */
	public void setName(String pName){
		this.name = pName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getId(){
		return this.id;
	}

	/**
	 * @return the dataType
	 */
	public UserDataType getDataType() {
		return dataType;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getDataTypeValue(){
		return this.dataType.getValue();
	}
	
	/**
	 * 
	 * @param pUserDataTypeValue
	 * @return
	 */
	public boolean isDataType(int pUserDataTypeValue){
		return (this.getDataTypeValue() == pUserDataTypeValue);
	}
	
	/**
	 * 
	 * @param pUserDataType
	 * @return
	 */
	public boolean isDataType(UserDataType pUserDataType){
		return this.isDataType(pUserDataType.getValue());
	}

	/**
	 * @return the parent
	 */
	public Object getParent() {
		return this.parent;
	}

	/*
	  @param parent the parent to set
	 
	public void setParent(Object parent) {
		this.parent = parent;
	}
	*/
}
