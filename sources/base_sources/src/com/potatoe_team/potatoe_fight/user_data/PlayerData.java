package com.potatoe_team.potatoe_fight.user_data;

import org.andengine.entity.Entity;

import com.badlogic.gdx.math.Vector2;

public class PlayerData extends ObjectData {

	public enum HealthState {
        GOOD,
        AVERAGE,
        BAD,
        REALLY_BAD,
        DEAD,
    }
	
	public enum TextMode{
		FULL,
		PERCENT,
	}
	
	public static final float ADMISSIBLE_FALL_DISTANCE = 250f; 
	
	private int weapon;
	private int skin;
	private long score;
	private int max_health;
	private int health;
	private int health_percent;
	private HealthState healthState;
	private TextMode textMode = TextMode.FULL;
	
	private String health_text;
	private String score_text = "Score\n";
	
	private Vector2 velocity = new Vector2(0,0);
	
	private boolean groundJump;
	private boolean airJump;
	private boolean onGround;
	private boolean doubleJump;
	private int jump_count;
	private Vector2 jump_pos_start;
	
	public PlayerData(Object parent){
		super(UserDataType.PLAYER, parent);
		this.max_health = this.health = 1000;
		this.update();
		this.jump_count = 0;
	}
	
	public PlayerData(String pName, Object parent){
		super(pName, UserDataType.PLAYER, parent);
		this.max_health = this.health = 1000;
		this.update();
		this.jump_count = 0;
	}

	/**
	 * @return the health
	 */
	public int health() {
		return health;
	}
	
	public int health_percent(){
		return health_percent;
	}
	
	public HealthState getState(){
		return this.healthState;
	}

	public void full_healing() {
		this.health = this.max_health;
		this.update();
	}
	
	
	public Boolean isAlive(){
		return (healthState != HealthState.DEAD) ? true : false;
	}
	
	/**
	 * 
	 */
	private void update() {
		this.health_percent = this.calc_percentage();
		int p = this.health_percent;
		if(p > 75){
			this.healthState = HealthState.GOOD;
		}else if(p > 50 && p <= 75){
			this.healthState = HealthState.AVERAGE;
		}else if(p > 25 && p <= 50){
			this.healthState = HealthState.BAD;
		}else if(p > 0 && p <= 25){
			this.healthState = HealthState.REALLY_BAD;
		}else if(p == 0){
			this.healthState = HealthState.DEAD;
		}
	}
	
	public String healthToText() {
		switch(textMode){
		case FULL:
			health_text = this.health + "/" +this.max_health;
			break;
		case PERCENT:
			health_text = this.health_percent + "%";
			break;
		}
		return health_text;
	}

	private int calc_percentage() {
		return (100 * this.health / this.max_health);
	}

	/**
	 * @param healing_value the amount of health to add
	 */
	public void heal(int healing_value) {
		this.health = Math.min(healing_value +  this.health, this.max_health);
		this.update();
	}

	/**
	 * @param damage_value the amount of health to remove
	 */
	public void damage(int damage_value) {
		this.health = Math.max(this.health - damage_value, 0);
		this.update();
	}

	/**
	 * @return the weapon
	 */
	public int getWeapon() {
		return weapon;
	}

	/**
	 * @param weapon the weapon to set
	 */
	public void setWeapon(int weapon) {
		this.weapon = weapon;
	}

	/**
	 * @return the skin
	 */
	public int getSkin() {
		return skin;
	}

	/**
	 * @param skin the skin to set
	 */
	public void setSkin(int skin) {
		this.skin = skin;
	}

	/**
	 * @return the score
	 */
	public long getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(long score) {
		this.score = score;
	}

	/**
	 * @return the velocity
	 */
	public Vector2 getVelocity() {
		return velocity;
	}

	/**
	 * @param velocity the velocity to set
	 */
	public void setVelocity(Vector2 pVelocity) {
		this.velocity = pVelocity;
	}

	/**
	 * @return the jump
	 */
	public boolean isJumping() {
		return (groundJump || airJump);
	}

	/**
	 * @param jump the jump to set
	 */
	private void setJumping(boolean pJump) {
		if(!pJump){
			this.groundJump = false;
			this.airJump = false;
		}
		else if(this.groundJump){
			this.groundJump = false;
			this.airJump = true;
			this.setDoubleJump(true);
		}
		else if(!this.groundJump && !this.airJump){
			this.groundJump = true;
		}
	}

	public void jump(Vector2 pJumpPos){
		this.jump_pos_start = pJumpPos;
		this.setJumping(true);
		this.jump_count ++;
	}
	
	public String ScoreToText() {
		return (score_text + score);
	}

	public int getJumpCount(){
		return this.jump_count;
	}
	
	public void reinitJump(){
		this.setJumping(false);
		this.setDoubleJump(false);
		this.jump_count = 0;
	}
	
	public float falling_distance(){
		return (this.jump_pos_start.y - ((Entity)this.getParent()).getY());
	}

	/**
	 * @return the airJump
	 */
	public boolean isAirJump() {
		return this.airJump;
	}
	
	/**
	 * @return the airJump
	 */
	public boolean isGroundJump() {
		return this.groundJump;
	}

	/**
	 * @return the onGround
	 */
	public boolean isOnGround() {
		return this.onGround;
	}

	/**
	 * @param onGround the onGround to set
	 */
	public void setOnGround(boolean pBool) {
		this.onGround = pBool;
		if(this.isOnGround()){
			this.reinitJump();
		}
	}

	public boolean isDoubleJump() {
		return this.doubleJump;
	}

	/**
	 * @param doubleJump the doubleJump to set
	 */
	public void setDoubleJump(boolean doubleJump) {
		this.doubleJump = doubleJump;
	}

}
