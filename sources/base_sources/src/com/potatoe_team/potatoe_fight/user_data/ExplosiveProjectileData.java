/**
 * 
 */
package com.potatoe_team.potatoe_fight.user_data;


/**
 * @author billy
 *
 */
public class ExplosiveProjectileData extends ObjectData {

	private float explosivePotential;
	
	private float velocity;
	
	/**
	 * 
	 */
	public ExplosiveProjectileData(Object parent) {
		super(UserDataType.EXPLOSIVE_PROJECTILE, parent);
	}

	/**
	 * @param pName
	 */
	public ExplosiveProjectileData(String pName, Object parent) {
		super(pName, UserDataType.EXPLOSIVE_PROJECTILE, parent);
	}

	/**
	 * @return the velocity
	 */
	public float getVelocity() {
		return velocity;
	}

	/**
	 * @param velocity the velocity to set
	 */
	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}

	/**
	 * @return the explosivePotential
	 */
	public float getExplosivePotential() {
		return explosivePotential;
	}

	/**
	 * @param explosivePotential the explosivePotential to set
	 */
	public void setExplosivePotential(float explosivePotential) {
		this.explosivePotential = explosivePotential;
	}

	/**
	 * 
	 * @return an explosion from the impact
	 */
	public ExplosionData impact(){
		return null;
		
	}
}
