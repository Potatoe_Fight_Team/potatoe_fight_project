/**
 * 
 */
package com.potatoe_team.potatoe_fight.map;

import java.util.ArrayList;

import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.potatoe_team.potatoe_fight.utils.geom2D.Circle;
import com.potatoe_team.potatoe_fight.utils.geom2D.Geom2D;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;

/**
 * @author billy
 *
 */
public class Ground {
	//Static ground body type
	public static final BodyType groundBodyType = BodyType.StaticBody;
	//definition of ground fixture (density, elasticity, friction)
	public static final FixtureDef groundFixtureDef = PhysicsFactory.createFixtureDef(1, 0.0f, 0.2f);
	
	public static GroundBlock createGroundBlock(ArrayList<Vector2> pVertices, TextureRegion groundTexture, VertexBufferObjectManager vbom){
		final float offsetX = 0f;
		final float offsetY = 0f;
		
		int nb_vertices = pVertices.size();
		final float[] Xvertex = new float[nb_vertices];
		final float[] Yvertex = new float[nb_vertices];
		
	    for( int i = 0; i < nb_vertices; i++) {
	    	   Xvertex[i] = pVertices.get(i).x;
	    	   Yvertex[i] = pVertices.get(i).y;
	    }
		final GroundBlock gb = new GroundBlock(offsetX, offsetY, Xvertex, Yvertex, groundTexture, vbom) ;
		gb.setVertices(pVertices);
		return gb;
	}
	
	public static void destruction(GroundBlock gb, Circle circle){
		Polygon groundPoly = Geom2D.createPolygon(gb.vertices());
		Polygon circlePoly = Geom2D.createPolyCircle(circle, 32);
		Geometry g = groundPoly.difference(circlePoly);
		gb.newGeom(g);
	}
	
	public static ArrayList<Vector2> getGeometryVertices(Geometry g){
		ArrayList<Vector2> l = new ArrayList<Vector2>();
		Coordinate[] coord = g.getCoordinates();
		for(int i=0; i < coord.length-1; i++){
			l.add(Geom2D.vector2FromCoord(coord[i]));
		}
		return l;
	}
	
}
