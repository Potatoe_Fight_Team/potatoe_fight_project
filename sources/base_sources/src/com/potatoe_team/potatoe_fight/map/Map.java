package com.potatoe_team.potatoe_fight.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.andengine.entity.scene.background.AutoParallaxBackground;
import org.andengine.entity.scene.background.ParallaxBackground.ParallaxEntity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;

/**
 * @author billy
 * Creation and manipulation of game map
 */
public class Map{
	//texture of the ground
	private TextureRegion midgroundTexture;
	//texture of the background
	private TextureRegion backgroundTexture;
	//create a parallax background
	private AutoParallaxBackground background = new AutoParallaxBackground(0, 0, 0, 0.5f);

	private PhysicsWorld physicsWorld;
	private VertexBufferObjectManager vbom;
	//ground formed by this list of entity 
	private List<GroundBlock> groundBlockList = new ArrayList<GroundBlock>();
	private  List<GroundBlock> updatedGroundBlockList = new ArrayList<GroundBlock>();
	public List<Vector2> spawnPoints = new ArrayList<Vector2>();
	
	public Vector2 dimension;
	public String name;
	public int id;
	public String ground_texture_path;
	public String background_texture_path;
	public List<List<Vector2>> List_vertices ;
	
	//	***	Constructors	*** 
	
	public Map(int map_id, String map_name, Vector2 map_dimension, String map_ground_texture_path, String map_background_texture_path, List<List<Vector2>> map_list_vertices, List<Vector2> spawn_points){
		this.id = map_id;
		this.name = map_name;
		this.dimension = map_dimension;
		this.ground_texture_path = map_ground_texture_path;
		this.background_texture_path = map_background_texture_path;
		this.List_vertices = map_list_vertices;
		this.spawnPoints = spawn_points;
	}
	
	//	***	Getter && Setter ***
	
	
	public void init(FixedStepPhysicsWorld pPhysicsWorld) {
		this.setBackgroundTexture(ResourcesManager.getInstance().game_background_region);
		this.setMidGroundTexture(ResourcesManager.getInstance().game_midground_region);
		this.setVertexObjectManager(ResourcesManager.getInstance().vbom);
		this.setPhysicsWorld(pPhysicsWorld);
		this.loadBackground();
		this.generateMidGround();
	}

	/**
	 * 
	 * @param pw physicsWorld
	 */
	private void setPhysicsWorld(PhysicsWorld pw){
		this.physicsWorld = pw;
	}
	
	
	/**
	 * @return width of the map
	 */
	public float getWidth() {
		return this.dimension.x;
	}
	
	/**
	 * @return the height of the map
	 */
	public float getHeight() {
		return this.dimension.y;
	}
	

	/**
	 * @return background of the map
	 */
	public AutoParallaxBackground getBackground() {
		return this.background;
	}
	
	/**
	 * (re)create the parallax background from backgroundTexture
	 */
	public void loadBackground() {
		Sprite bg = new Sprite(0, 0, this.backgroundTexture, this.vbom);
		bg.setOffsetCenter(0, 0);
		this.background.attachParallaxEntity(new ParallaxEntity(0.0f, bg));
	}
	
	/**
	 * @return List of TexturedPolygon
	 */
	public List<GroundBlock> getMidGround(){
		return this.groundBlockList;
	}
	

	/**
	 * generate the list of TexturedPolygon representing the ground
	 */
	public void generateMidGround(){
		this.groundBlockList = new ArrayList<GroundBlock>();
		Iterator<List<Vector2>> i = List_vertices.iterator();
		while(i.hasNext()){
			ArrayList<Vector2> v_list = (ArrayList<Vector2>)i.next();
			GroundBlock gb = Ground.createGroundBlock(v_list, this.midgroundTexture, this.vbom);
			gb.createBody(this.physicsWorld);
			this.groundBlockList.add(gb);
		}
	}

	/**
	 * apply change on ground
	 */
	public void updateMidGround(){
		this.updatedGroundBlockList = new ArrayList<GroundBlock>();
		for(int i=0; i < this.groundBlockList.size(); i++){
			GroundBlock gb = this.groundBlockList.get(i);
			if(gb.need_update()){
				gb.destructBody(this.physicsWorld);
				this.groundBlockList.remove(i);
				i--;
				for(int j = 0; j < gb.getNumGeometryEntities(); j++){
					final GroundBlock new_gb = Ground.createGroundBlock(gb.getGeometryEntityVertices(j), this.midgroundTexture, this.vbom);
					new_gb.createBody(this.physicsWorld);
					this.groundBlockList.add(new_gb);
					this.updatedGroundBlockList.add(new_gb);
				}
			}
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public List<GroundBlock> updatedGroundBlocks(){
		return this.updatedGroundBlockList;
	}

	/**
	 * 
	 * @param game_background_region
	 */
	private void setBackgroundTexture(TextureRegion game_background_region) {
		this.backgroundTexture = game_background_region;
	}

	/**
	 * 
	 * @param game_midground_region
	 */
	private void setMidGroundTexture(TextureRegion game_midground_region) {
		this.midgroundTexture = game_midground_region;
	}

	/**
	 * 
	 * @param pvbom
	 */
	private void setVertexObjectManager(VertexBufferObjectManager pvbom) {
		this.vbom = pvbom;
		
	}
	
	public boolean need_update(){
		boolean update = false;
		Iterator<GroundBlock> i = this.groundBlockList.iterator();
		while(i.hasNext()){
			GroundBlock gb = i.next();
			if (gb.need_update()){
				update = true;
				break;
			}
		}
		return update;
	}
	
	//	***	Methods	***
	
}
