/**
 * 
 */
package com.potatoe_team.potatoe_fight.map;

import org.andengine.util.debug.Debug;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.player.Patate;
import com.potatoe_team.potatoe_fight.scene.MainGameScene;
import com.potatoe_team.potatoe_fight.user_data.ExplosionData;
import com.potatoe_team.potatoe_fight.user_data.ObjectData;
import com.potatoe_team.potatoe_fight.user_data.PlayerData;
import com.potatoe_team.potatoe_fight.utils.geom2D.Circle;
import com.potatoe_team.potatoe_fight.utils.geom2D.Geom2D;
import com.potatoe_team.potatoe_fight.weapon.Bullet;
import com.potatoe_team.potatoe_fight.weapon.Explosion;

/**
 * @author billy
 *
 */
public class GameContactListener implements ContactListener {
	
	private final MainGameScene game_scene;
	
	private int last_explosion = -1;
	
	private int last_ground = -1;
	/**
	 * 
	 */
	public GameContactListener(MainGameScene s) {
		this.game_scene = s;
	}
	
	@Override
	public void beginContact(Contact contact) {
	    final Fixture x1 = contact.getFixtureA();
        final Fixture x2 = contact.getFixtureB();
    	final ObjectData x1Data = (ObjectData) x1.getBody().getUserData();
    	final ObjectData x2Data = (ObjectData) x2.getBody().getUserData();
    	
        if (x1Data != null && x2Data != null){
        	switch(x1Data.getDataType()){
				case EXPLOSION:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							if(!(last_explosion == x1Data.getId() && last_ground == x2Data.getId())){
								last_explosion = x1Data.getId(); last_ground = x2Data.getId();
								GroundBlock gb = (GroundBlock)(x2Data.getParent());
								Vector2 center = ((ExplosionData)x1Data).getCenter();
								float radius = ((ExplosionData)x1Data).getRadius();
								Circle circle = Geom2D.createCircle(center, radius);
								Ground.destruction(gb, circle);
								((Explosion)x1Data.getParent()).destroyBody();
							}
							Debug.e("potato", "boom!");
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case EXPLOSIVE_PROJECTILE:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case GROUND:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							if(!(last_explosion == x2Data.getId() && last_ground == x1Data.getId())){
								last_explosion = x2Data.getId(); last_ground = x1Data.getId();
								GroundBlock gb = (GroundBlock)x1Data.getParent();
								Vector2 center = ((ExplosionData)x2Data).getCenter();
								float radius = ((ExplosionData)x2Data).getRadius();
								Circle circle = Geom2D.createCircle(center, radius);
								Ground.destruction(gb, circle);
								((Explosion)x2Data.getParent()).destroyBody();
							}
							Debug.e("potato", "boom!");
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							break;
						case PLAYER:
							if(((PlayerData)x2Data).isJumping() && ((PlayerData)x2Data).falling_distance() > PlayerData.ADMISSIBLE_FALL_DISTANCE){
								((PlayerData)x2Data).damage(Math.max(0, (int)(((PlayerData)x2Data).falling_distance() / 9.81f)));
								game_scene.update_healthbar();
							}
							((PlayerData)x2Data).setOnGround(true);
							Patate p = (Patate)(((PlayerData)x2Data).getParent());
							break;
						case PROJECTILE:
							//final Explosion e = new Explosion(x2.getBody().getPosition().x, x2.getBody().getPosition().y, 30);
							//game_scene.addExplosion(e);
							final Bullet b = (Bullet)(x2Data.getParent());
							b.setVisible(false);
							x2.setSensor(true);
							ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){
								@Override
								public void run() {
									b.destroy();
								}
							});	
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case PLAYER:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							Debug.e("potato", "contact player explosion");
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							if(((PlayerData)x2Data).isJumping() && ((PlayerData)x1Data).falling_distance() > PlayerData.ADMISSIBLE_FALL_DISTANCE){
								((PlayerData)x1Data).damage(Math.max(0, (int)(((PlayerData)x1Data).falling_distance() / 9.81f)));
								game_scene.update_healthbar();
							}
							((PlayerData)x1Data).setOnGround(true);
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case PROJECTILE:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							//final Explosion e = new Explosion(x1.getBody().getPosition().x, x1.getBody().getPosition().y, 30);
							//game_scene.addExplosion(e);
							final Bullet b = (Bullet)(x1Data.getParent());
							b.setVisible(false);
							x1.setSensor(true);
							ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){
								@Override
								public void run() {
									b.destroy();
								}
							});	
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case UNKNOWN:
						switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				default:
					break;
        	}
    	}
	}

	@Override
	public void endContact(Contact contact) {
		final Fixture x1 = contact.getFixtureA();
        final Fixture x2 = contact.getFixtureB();
    	final ObjectData x1Data = (ObjectData) x1.getBody().getUserData();
    	final ObjectData x2Data = (ObjectData) x2.getBody().getUserData();
        
        if (x1Data != null && x2Data != null){
        	switch(x1Data.getDataType()){
				case EXPLOSION:
					Debug.e("potato", "end explosion!");
					switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case EXPLOSIVE_PROJECTILE:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case GROUND:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							break;
						case PLAYER:
							((PlayerData)x2Data).setOnGround(false);
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case PLAYER:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							((PlayerData)x1Data).setOnGround(false);
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case PROJECTILE:
					switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				case UNKNOWN:
						switch(x2Data.getDataType()){
						case EXPLOSION:
							break;
						case EXPLOSIVE_PROJECTILE:
							break;
						case GROUND:
							break;
						case PLAYER:
							break;
						case PROJECTILE:
							break;
						case UNKNOWN:
							break;
						default:
							break;
					}
					break;
				default:
					break;
        	}
        }
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
	}

}
