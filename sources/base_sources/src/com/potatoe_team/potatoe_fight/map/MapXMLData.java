/**
 * 
 */
package com.potatoe_team.potatoe_fight.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import com.badlogic.gdx.math.Vector2;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;

/**
 * @author billy
 * Load, save and manage data about a map
 */
public class MapXMLData {
	
	private static final String map_extension = ".xml";
	private static final String maps_path = "data/maps/";
	private static final String lastmap_url = maps_path + "lastmap" + map_extension;
	private static final String maplist_url = maps_path + "list" + map_extension;
	
	private static org.jdom2.Document document;
	private static Element racine;
	
	private static int map_id;
	private static String map_name;
	private static Vector2 map_dimension;
	private static String map_ground_texture_path;
	private static String map_background_texture_path;
	private static List<List<Vector2>> List_vertices ; 
	
	public static Map load_lastmapXMLData(){
		return load_map_from_url(lastmap_url);
	}
	
	public static Map load_mapXMLData(int map_id){
		return load_map_from_url(maps_path + map_id + map_extension);
	}
	
	//
	// Private functions
	//
	
	public static int search_map_id(String map_name){
		int id = -1;
		SAXBuilder sxb = new SAXBuilder();
		try
		{
		    document = sxb.build(ResourcesManager.getInstance().activity.getAssets().open(maplist_url));
		}
		catch(Exception e){}
		 
		racine = document.getRootElement();
		 
		List<Element> listMap = racine.getChildren("map");
		
		Iterator<Element> i = listMap.iterator();
		while(i.hasNext())
		{
		   Element e = i.next();
		   if( map_name.equals(e.getChild("map_name").getText()) ){
			   String s_id = e.getChild("map_id").getText();
			   id = Integer.parseInt(s_id);
			   break;
		   }
		}
		
		return id;
	}
	
	/*
	 * get all list ids
	 */
	public static int[] maps_id_list(){
		SAXBuilder sxb = new SAXBuilder();
		try
		{
		    document = sxb.build(ResourcesManager.getInstance().activity.getAssets().open(maplist_url));
		}
		catch(Exception e){}
		 
		racine = document.getRootElement();
		 
		List<Element> listMap = racine.getChildren("map");
		int[] id_list = new int[listMap.size()];
		Iterator<Element> i = listMap.iterator();
		int a = 0;
		while(i.hasNext())
		{
		   id_list[a] = Integer.parseInt(i.next().getChild("map_id").getText());
		   a++;
		}
		return id_list;
	}
	
	/**
	 * load map data from a xml file path
	 */
	public static Map load_map_from_url(String map_path){
				
		SAXBuilder sxb = new SAXBuilder();
		try
		{
		    document = sxb.build(ResourcesManager.getInstance().activity.getAssets().open(map_path));
		}
		catch(Exception e){}
		
		racine = document.getRootElement();
		String s_id = racine.getChild("map_id").getText();
		map_id = Integer.parseInt(s_id);
		map_name = racine.getChild("map_name").getText();
		int width = (int)Double.parseDouble(racine.getChild("map_dimension").getChild("width").getText());
		int height = (int)Double.parseDouble(racine.getChild("map_dimension").getChild("height").getText());
		map_dimension = new Vector2(width, height);
		map_ground_texture_path = racine.getChild("map_ground_texture_path").getText();
		map_background_texture_path = racine.getChild("map_background_texture_path").getText();

		List<Element> listMapGroundBlock = racine.getChild("map_ground_block_list").getChildren("map_ground_block");
		List_vertices = new ArrayList<List<Vector2>>(listMapGroundBlock.size());
		Iterator<Element> i = listMapGroundBlock.iterator();
		while(i.hasNext())
		{
		   Element e = i.next();
		   ArrayList<Vector2> vertices = new ArrayList<Vector2>();
		   List<Element> listVertice = e.getChildren("vertex");
		   Iterator<Element> j = listVertice.iterator();
		   while(j.hasNext()){
			   Element f = j.next();
			   float x = Float.parseFloat(f.getChild("x").getText());
			   float y = Float.parseFloat(f.getChild("y").getText());
			   Vector2 vertex = new Vector2(x, y);
			   vertices.add(vertex);
		   }
		   List_vertices.add(vertices);
		}
		
		List<Element> listSpawnPoints = racine.getChild("spawn_point_list").getChildren("spawn_point");
		List<Vector2> spawn_points = new ArrayList<Vector2>(listSpawnPoints.size());
		Iterator<Element> j = listSpawnPoints.iterator();
		while(j.hasNext())
		{
			   Element f = j.next();
			   float x = Float.parseFloat(f.getChild("x").getText());
			   float y = Float.parseFloat(f.getChild("y").getText());
			   Vector2 vertex = new Vector2(x, y);
			   spawn_points.add(vertex);
		}
		
		return new Map(map_id, map_name, map_dimension, map_ground_texture_path, map_background_texture_path, List_vertices, spawn_points);
	}
	
}
