package com.potatoe_team.potatoe_fight.map;

import org.andengine.util.debug.Debug;



/**
 * 
 * @author billy
 *
 */
public class WindBlowingManager {

	public enum WindDirection {
		STRONG_LEFT,
		SOFT_LEFT,
		NONE,
		SOFT_RIGHT,
		STRONG_RIGHT,
	}
	
	private WindDirection wind_direction = WindDirection.NONE;
	private int wind = 0;
	private int wind_force_value = 0;
	private int wind_direction_value = 0;
	
	
	public void update_wind(){
		//TODO algorithme changement du vent
		/*
		 * si le vent souffle fort de la gauche alors random entre -6 et 0 avec plus de proba d'être entre -6 et -2
		 * si le vent souffle normalement de la gauche alors random entre -6 et 0 avec plus de proba d'être entre -3 et 0
		 * si le vent ne souffle pas alors random entre -6 et 6 avec plus de probal d'être entre -4 et 4
		 * si le vent souffle fort de la droite alors random entre 6 et 0 avec plus de proba d'être entre 6 et 2
		 * si le vent souffle normalement de la droite alors random entre 6 et 0 avec plus de proba d'être entre 3 et 0
		 */
		
		double tmp = Math.random();
		switch(wind_direction){
		case NONE:
			wind_force_value = (int)(tmp * 5);
			if((tmp + 0.5f) > 1){
				wind_direction_value = -1;
			}else{
				wind_direction_value = 1;
			}
			break;
		case SOFT_LEFT:
			if(tmp < 0.75f){
				wind_force_value = (int)(tmp * 4);
			}else{
				tmp = Math.random();
				wind_force_value = (int)(tmp * 7);
			}
			wind_direction_value = 1;
		case STRONG_LEFT:
			if(tmp < 0.6f){
				wind_force_value = (int)(tmp * 5) + 2;
			}else{
				tmp = Math.random();
				wind_force_value = (int)(tmp * 7);
			}
			wind_direction_value = 1;
			break;
		case SOFT_RIGHT:
			if(tmp < 0.75f){
				wind_force_value = (int)(tmp * 4);
			}else{
				tmp = Math.random();
				wind_force_value = (int)(tmp * 7);
			}
			wind_direction_value = -1;
		case STRONG_RIGHT:
			if(tmp < 0.6f){
				wind_force_value = (int)(tmp * 5) + 2;
			}else{
				tmp = Math.random();
				wind_force_value = (int)(tmp * 7);
			}
			wind_direction_value = -1;
			break;
		default:
			break;
		}
		if(wind_force_value >= 4){
			wind_direction = (wind_direction_value < 0) ? WindDirection.STRONG_LEFT : WindDirection.STRONG_RIGHT;
		}else if(wind_force_value > 1){
			wind_direction = (wind_direction_value < 0) ? WindDirection.SOFT_LEFT : WindDirection.SOFT_RIGHT;
		}else{
			wind_direction = WindDirection.NONE;
		}
		
		wind = wind_force_value * wind_direction_value;
		
	}
	
	public WindDirection getWindDirection(){
		return wind_direction;
	}
	
	public int getWindForce(){
		return wind;
	}

}
