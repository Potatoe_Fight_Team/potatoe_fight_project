/**
 * 
 */
package com.potatoe_team.potatoe_fight.map;

import java.util.ArrayList;

import org.andengine.entity.shape.IShape;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.user_data.GroundData;
import com.potatoe_team.potatoe_fight.utils.TexturedPolygon;
import com.vividsolutions.jts.geom.Geometry;

/**
 * @author billy
 *
 */
public class GroundBlock extends TexturedPolygon{
	private Body body;
	private GroundData mData;
	private ArrayList<Vector2> vertices;
	private boolean modified = false;
	
	private Geometry modifiedGeom;

	/**
	 * 
	 * @param offsetX
	 * @param offsetY
	 * @param pVerticesX
	 * @param pVerticesY
	 * @param pTexture
	 * @param vbom
	 */
	public GroundBlock(float offsetX, float offsetY, float[] pVerticesX,
			float[] pVerticesY, TextureRegion pTexture,
			VertexBufferObjectManager vbom) {
		super(offsetX, offsetY, pVerticesX, pVerticesY,  pTexture, vbom);
		mData = new GroundData(this);
		this.setUserData(mData);
	}

	/**
	 * 
	 * @param pPhysicsWorld
	 * @return
	 */
	public Body createBody(PhysicsWorld pPhysicsWorld){
		this.mData.setVertices(this.vertices);
		final Vector2[] body_vertices = new Vector2[this.vertices.size()];
		for(int i = 0; i < body_vertices.length; i++){
			body_vertices[i] = new Vector2();
			body_vertices[i].x = this.vertices(i).x / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;
			body_vertices[i].y = this.vertices(i).y / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;
		}

		this.body = PhysicsFactory.createLoopChainBody(pPhysicsWorld, body_vertices, Ground.groundBodyType, Ground.groundFixtureDef);
		this.body.setUserData(mData);
		pPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(this, body, true, true));
		return this.body;
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Vector2> vertices(){
		return this.vertices;
	}
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public Vector2 vertices(int index){
		return this.vertices.get(index);
	}
	
	/**
	 * 
	 * @param pVertices
	 */
	public void setVertices(ArrayList<Vector2> pVertices){
		this.vertices = pVertices;
		//this.modified = true;
	}
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public Vector2 getVertexAtIndex(int index){
		return this.vertices.get(index);
	}
	
	/**
	 * 
	 * @param vertex
	 * @return
	 */
	public int getVertexIndex(Vector2 vertex){
		return this.vertices.indexOf(vertex);
	}
	
	/**
	 * 
	 * @param vertex
	 */
	public void addVertex(Vector2 vertex){
		this.vertices.add(vertex);
	}
	
	/**
	 * 
	 * @param vertex
	 * @param index
	 */
	public void addVertexAtIndex(Vector2 vertex, int index){
		this.vertices.add(index, vertex);
	}
	
	/**
	 * 
	 * @param vertex
	 */
	public void removeVertex(Vector2 vertex){
		this.vertices.remove(vertex);
	}
	
	/**
	 * 
	 * @param index
	 */
	public void removeVertexeAtIndex(int index){		
		this.vertices.remove(index);
	}
	
	/**
	 * remove all vertices between the start_index and the end_index included
	 * @param start_index
	 * @param end_index
	 */
	public void removeSubVertices(int start_index, int end_index){
		if(start_index <= end_index 
				&& start_index >= 0 && start_index < this.vertices.size()
				&& end_index >= 0 && end_index < this.vertices.size()){
			int i = start_index;
			int nb_suppr = end_index - start_index;
			while(nb_suppr > 0){
				this.vertices.remove(i);
				nb_suppr--;
			}
			this.modified = true;
		}
	}
	
	/**
	 * 
	 * @param new_vertices
	 */
	public void addVertices(ArrayList<Vector2> new_vertices){
		for(int i = 0; i < new_vertices.size() ; i++){
			this.vertices.add(new_vertices.get(i));
		}
		this.modified = true;
	}
	
	/**
	 * 
	 * @param index
	 * @param new_vertices
	 */
	public void insertVertices(int index, ArrayList<Vector2> new_vertices){
		for(int i = index, j = 0; i < index+new_vertices.size(); i++, j++){
			this.vertices.add(i, new_vertices.get(j));
		}
		this.modified = true;
	}
	
	/**
	 * 
	 * @return
	 */
	public Body getBody(){
		return body;
	}
	
	/**
	 * 
	 * @param pPhysicsWorld
	 */
	public void destructBody(final PhysicsWorld pPhysicsWorld){
		if(this.body != null){
			final IShape s = this;
			body.setActive(false);
			ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){
				@Override
				public void run() {
					final PhysicsConnector physicsConnector = pPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(s);
					pPhysicsWorld.unregisterPhysicsConnector(physicsConnector );
					ResourcesManager.getInstance().activity.getEngine().unregisterUpdateHandler(s);
					if(body != null){
						body.setActive(false);	
						pPhysicsWorld.destroyBody(body);
						body = null;
					}
					detachSelf();
				}
				
			});
		}
		this.modified = true;
	}
	
	/**
	 * 
	 */
	public void destruct(){
		this.vertices.clear();
		this.modified = true;
	}

	public boolean need_update() {
		return this.modified ;
	}
	
	/**
	 * 
	 * @param n
	 * @return
	 */
	public ArrayList<Vector2> getGeometryEntityVertices(int n){
		ArrayList<Vector2> res = new ArrayList<Vector2>();
		if(this.modifiedGeom.getNumGeometries() > n){
			Geometry tmp = this.modifiedGeom.getGeometryN(n);
			res = Ground.getGeometryVertices(tmp);
		}
		return res;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getNumGeometryEntities(){
		return this.modifiedGeom.getNumGeometries();
	}
	
	/**
	 * 
	 * @return
	 */
	public Geometry getModifiedGeom(){
		return this.modifiedGeom;
	}
	
	/**
	 * 
	 * @param g
	 */
	public void newGeom(Geometry g){
		this.modifiedGeom = g;
		this.modified =true;
	}
	
}
