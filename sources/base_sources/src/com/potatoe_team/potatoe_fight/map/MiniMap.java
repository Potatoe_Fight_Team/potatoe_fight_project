/**
 * 
 */
package com.potatoe_team.potatoe_fight.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.util.adt.color.Color;

import com.badlogic.gdx.math.Vector2;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.utils.PositionIndicator;
import com.potatoe_team.potatoe_fight.utils.TexturedPolygon;

/**
 * @author billy
 *
 */
public class MiniMap extends Rectangle{

	public enum MinimapState{
		HIDE,
		SHOW,
	}
	
	private float map_scale;
	private float map_alpha = 0.7f;
	private ArrayList<TexturedPolygon> ground;
	private Vector2 player;
	private ArrayList<Vector2> allies = new ArrayList<Vector2>();
	private ArrayList<Vector2> enemies = new ArrayList<Vector2>();
	private MinimapState state;
	private ResourcesManager resources;
	/**
	 * 
	 */
	public MiniMap(float pX, float pY, float pWidth, float pHeight, ResourcesManager pResourcesManager){
		this(pX, pY, pWidth, pHeight, pResourcesManager, 0.2f);
	}
	
	public MiniMap(float pX, float pY, float pWidth, float pHeight, ResourcesManager pResourcesManager, float pScale){
		super(pX, pY, pWidth, pHeight, pResourcesManager.vbom);
		this.resources = pResourcesManager;
		this.setAnchorCenter(0, 0);
		this.map_scale = pScale;
		this.setColor(Color.WHITE_ABGR_PACKED_INT);
		this.setAlpha(0.6f);
		this.hide();
	}
	
	public void changeState(){
		switch(this.state){
		case HIDE:
			this.show();
			break;
		case SHOW:
			this.hide();
			break;
		default:
			break;
		}
	}
	
	public void show() {
		this.state = MinimapState.SHOW;
		this.setVisible(true);
	}

	public void hide(){
		this.state = MinimapState.HIDE;
		this.setVisible(false);
	}
	
	public boolean isHidden(){
		switch(this.state){
		case HIDE:
			return true;
		case SHOW:
			return false;
		default:
			return false;
		}
	}
	
	public boolean isShown(){
		switch(this.state){
		case HIDE:
			return false;
		case SHOW:
			return true;
		default:
			return false;
		}
	}
	
	
	public void setGround(List<GroundBlock> list){
		this.ground = new ArrayList<TexturedPolygon>();
		Iterator<GroundBlock> i = list.iterator();
		while (i.hasNext()) {
			TexturedPolygon s = (TexturedPolygon)i.next();
			this.ground.add(new TexturedPolygon(s.getX()*this.map_scale, s.getY()*this.map_scale, s.getVertexX(), s.getVertexY(), s.getTextureRegion(), this.getVertexBufferObjectManager()));
		}
		
		Iterator<TexturedPolygon>j = this.ground.iterator();
		while(j.hasNext()){
			TexturedPolygon s = j.next();
			s.setScale(this.map_scale);
			s.setAlpha(this.map_alpha);
		}
	}
	
	public void setPlayer(Vector2 pos){
		this.player = new Vector2(pos.x*this.map_scale, pos.y*this.map_scale);
	}
	
	public void setEnemie(int index, Vector2 pos){
		this.enemies.set(index, new Vector2(pos.x*this.map_scale, pos.y*this.map_scale));
	}
	
	public void setAllie(int index, Vector2 pos){
		this.allies.set(index, new Vector2(pos.x*this.map_scale, pos.y*this.map_scale));
	}
	
	public int addEnemie(Vector2 pos){
		this.enemies.add(new Vector2(pos.x*this.map_scale, pos.y*this.map_scale));
		return this.enemies.size() - 1;
	}
	
	public int addAllie(Vector2 pos){
		this.allies.add(new Vector2(pos.x*this.map_scale, pos.y*this.map_scale));
		return this.allies.size() - 1;
	}
	
	public void removeEnemie(int index){
		this.enemies.remove(index);
	}
	
	public void removeAllie(int index){
		this.allies.remove(index);
	}

	public void update(){
		this.detachChildren();
		Iterator<TexturedPolygon> i = this.ground.iterator();
		while(i.hasNext()){
			TexturedPolygon s = i.next();
			this.attachChild(s);
		}
		Iterator<Vector2> j = this.allies.iterator();
		while(j.hasNext()){
			final PositionIndicator ally_pin = new PositionIndicator(j.next(), this.resources.allies_indicator_texture_region, this.resources.vbom);
			this.drawIndicator(ally_pin);
		}
		j = this.enemies.iterator();
		while(j.hasNext()){
			final PositionIndicator enemy_pin = new PositionIndicator(j.next(), this.resources.enemies_indicator_texture_region, this.resources.vbom);
			this.drawIndicator(enemy_pin);
		}
		final PositionIndicator player_pin = new PositionIndicator(this.player, this.resources.player_indicator_texture_region, this.resources.vbom);
		this.drawIndicator(player_pin);
	}
	
	private void drawIndicator(PositionIndicator p){
		if((p.getX() - p.getWidth()/2) > this.getWidth() 
			|| (p.getX() + p.getWidth()/2) < 0 
			|| (p.getY() + p.getHeight()/2) < 0 
			|| (p.getY()-p.getHeight()/2) > this.getHeight()){
			p.setVisible(false);
		}
		p.setAnchorCenterY(0);
		p.setAlpha(map_alpha);
		this.attachChild(p);
	}
}
