package com.potatoe_team.potatoe_fight.player;

import org.andengine.entity.Entity;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.weapon.Gun;
import com.potatoe_team.potatoe_fight.weapon.Bullet;
import com.potatoe_team.potatoe_fight.user_data.ObjectData;
import com.potatoe_team.potatoe_fight.user_data.PlayerData;
import com.potatoe_team.potatoe_fight.weapon.Weapon;
import com.potatoe_team.potatoe_fight.weapon.WeaponList.WeaponType;

public class Patate extends AnimatedSprite{
	private Body mActiveBody;
	private Body mLeftModeBody;
	private Body mRightModeBody;
	private PlayerData mData;
	private final float scale_val = 0.8f;
	private final float ptmr = PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;
	private FixedStepPhysicsWorld mPhysicsWorld;
	private int direction = 1;
	private boolean canRun = true;
	private Entity layer;
	
	private final Vector2[] vertices_right_mode = {
			new Vector2(33.0f * scale_val / ptmr, 26.0f * scale_val / ptmr),
			new Vector2(7.0f * scale_val / ptmr, 42.0f * scale_val / ptmr),
			new Vector2(-13.0f * scale_val / ptmr, 37.0f * scale_val / ptmr),
			new Vector2(-26.0f * scale_val / ptmr, -20.0f * scale_val / ptmr),
			new Vector2(-16.0f * scale_val / ptmr, -36.0f * scale_val / ptmr),
			new Vector2(2.0f * scale_val / ptmr, -45.0f * scale_val / ptmr),
			new Vector2(19.0f * scale_val / ptmr, -44.0f * scale_val / ptmr),
			new Vector2(36.0f * scale_val / ptmr, -29.0f * scale_val / ptmr)
	};
	private final Vector2[] vertices_left_mode = {
			new Vector2(-36.0f * scale_val / ptmr, -29.0f * scale_val / ptmr),
			new Vector2(-19.0f * scale_val / ptmr, -44.0f * scale_val / ptmr),
			new Vector2(-2.0f * scale_val / ptmr, -45.0f * scale_val / ptmr),
			new Vector2(16.0f * scale_val / ptmr, -36.0f * scale_val / ptmr),
			new Vector2(26.0f * scale_val / ptmr, -20.0f * scale_val / ptmr),
			new Vector2(13.0f * scale_val / ptmr, 37.0f * scale_val / ptmr),
			new Vector2(-7.0f * scale_val / ptmr, 42.0f * scale_val / ptmr),
			new Vector2(-33.0f * scale_val / ptmr, 26.0f * scale_val / ptmr)
	};
	private Weapon current_weapon;
	private RevoluteJointDef revoluteJointDef;
	private Joint weaponJoint;
	private int call;
	
	public Patate(float pX, float pY, VertexBufferObjectManager vbom, FixedStepPhysicsWorld pPhysicsWorld) {
		
        super(pX, pY, ResourcesManager.getInstance().patate_region, vbom);
        mPhysicsWorld = pPhysicsWorld;
        mData = new PlayerData(this);
        this.setUserData(mData);
		this.setScale(scale_val);		
		this.createBody(mPhysicsWorld);   
    }
	
public void changeDirection(){
		
		direction *= -1;
		
		final AnimatedSprite shape = this;
	
		if(this.goRight()){
			setFlippedHorizontal(false);
			this.mRightModeBody.setTransform(this.mActiveBody.getPosition(), this.mActiveBody.getAngle());
			this.mRightModeBody.setActive(true);
			this.mActiveBody = this.mRightModeBody;
		}else{
			setFlippedHorizontal(true);
			this.mLeftModeBody.setTransform(this.mActiveBody.getPosition(), this.mActiveBody.getAngle());
			this.mLeftModeBody.setActive(true);
			this.mActiveBody = this.mLeftModeBody;
		}
		ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){
			@Override
			public void run() {
				if(mPhysicsWorld != null && mActiveBody != null){
					mPhysicsWorld.unregisterPhysicsConnector(mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(shape));
					mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(shape, mActiveBody, true, true));
				}
			}
		});
		if(current_weapon != null){
			this.changeWeapon(current_weapon.type);
			Debug.e("potato", "change weapon");
		}else{
			Debug.e("potato", "weapon null");
		}
		if(this.goRight()){
			this.mLeftModeBody.setActive(false);
		}else{
			this.mRightModeBody.setActive(false);
		}
		
	}
	
	public void onDie() {
		final AnimatedSprite shape = this;
		this.destroyWeapon();
		ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){
				@Override
				public void run() {
						
					mPhysicsWorld.unregisterPhysicsConnector(mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(shape));
					mPhysicsWorld.destroyBody(mRightModeBody);
					mPhysicsWorld.destroyBody(mLeftModeBody);
					mRightModeBody = null;
					mLeftModeBody = null;
					
					shape.detachSelf();
					shape.dispose();
				}
		});	
	}
	

	public int getDirection() {
		
		return direction;
	}
	
	public boolean goRight() {
		
		if(direction == 1)
			return true;
		else
			return false;
	}
	
	public boolean goLeft() {
		
		if(direction == -1)
			return true;
		else
			return false;
	}
	
	public void startRun(boolean can) {
		
		if(canRun == true && can == true) {
			canRun = false;
			setRunning(true);
		}
		if(can == false) {
			canRun = true;
			setRunning(false);
		}
	}
	
	public void setRunning(boolean isrunning) {

		final long[] PLAYER_ANIMATE = new long[] { 100, 100, 100 };
        
	    this.animate(PLAYER_ANIMATE, 0, 2, isrunning);
	}//setRunning
	

	public void jump() {
		if(mActiveBody != null && !this.mData.isDoubleJump()){
			this.mData.jump(new Vector2(this.getX(), this.getY()));
			mActiveBody.setLinearVelocity(new Vector2(mActiveBody.getLinearVelocity().x, 30.0f));
			this.animate(new long[] { 100, 600, 200, 100, 10 }, new int[] { 1, 2, 0, 1, 0 }, false);
		}
	}

	/**
	 * @return the mBody
	 */
	public Body getBody() {
		
		return mActiveBody;
	}

	
	private void createBody(FixedStepPhysicsWorld pPhysicsWorld){
		this.mRightModeBody = PhysicsFactory.createPolygonBody(pPhysicsWorld, this, vertices_right_mode, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 1.0f));
		this.mLeftModeBody = PhysicsFactory.createPolygonBody(pPhysicsWorld, this, vertices_left_mode, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 1.0f));
		this.mRightModeBody.setUserData(this.mData);
		this.mLeftModeBody.setUserData(this.mData);
		if(this.goLeft()){
			this.mActiveBody = this.mLeftModeBody;
			this.mLeftModeBody.setActive(true);
			this.mRightModeBody.setActive(false);
		}else{
			this.mActiveBody = this.mRightModeBody;
			this.mLeftModeBody.setActive(false);
			this.mRightModeBody.setActive(true);
		}
		mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(this, this.mActiveBody, true, true));
		
	}
	
	public void changeWeapon(WeaponType weaponType){
		
		final float pos_x;
		final float pos_y;
		
		final WeaponType p =  weaponType;
				
		switch(p){
		case GRENADE:
			break;
		case GUN:
			if(this.goRight()){
				pos_x = this.getX() +50;
			}else{
				pos_x = this.getX() -50;
			}
			
			ResourcesManager.getInstance().activity.runOnUpdateThread(new Runnable(){
				@Override
				public void run() {
					destroyWeapon();
					Gun gun = new Gun(pos_x, getY(), ResourcesManager.getInstance().vbom, ResourcesManager.getInstance().camera, ResourcesManager.getInstance().arme_region);
					Body gun_body = gun.createBody(mPhysicsWorld);
					mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(gun, gun_body, true, true));
					attachWeapon(gun);
					current_weapon = (Weapon)gun;
					current_weapon.setFlippedHorizontal(!goRight());
					layer.attachChild(current_weapon);
				}
			});
			
			break;
		case KNIFE:
			break;
		case ROCKET:
			break;
		default:
			break;
			
		}

	}
	
	public void destroyWeapon(){
		if(current_weapon != null){
			if(mPhysicsWorld != null && current_weapon.getBody() != null){
				if(weaponJoint != null){
					mPhysicsWorld.destroyJoint(weaponJoint);
					Debug.e("potato", "joint destroy");
				}
				weaponJoint = null;
				Body b = current_weapon.getBody();
				mPhysicsWorld.unregisterPhysicsConnector(mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(current_weapon));
				mPhysicsWorld.destroyBody(b);
				current_weapon.detachSelf();
				current_weapon.dispose();
				current_weapon = null;
				Debug.e("potato", "weapon destroy");
			}
		}
	}
	
	public void attachWeapon(Weapon wp){
		revoluteJointDef = new RevoluteJointDef();
		revoluteJointDef.initialize(mActiveBody, wp.getBody(), mActiveBody.getWorldCenter());
		revoluteJointDef.collideConnected = true;
		weaponJoint = mPhysicsWorld.createJoint(revoluteJointDef);
	}

	public void attack() {
		Sprite s = null;
		switch(this.current_weapon.type){
		case GRENADE:
			break;
		case GUN:
			Bullet bullet;
			float move_x ;
			float move_y = current_weapon.getY()+5;
			if(this.goLeft()) {
				move_x = current_weapon.getX() - 20;
			}
			else {
				move_x = current_weapon.getX() + 20;
			}
			bullet = new Bullet(move_x, move_y, ResourcesManager.getInstance().vbom, ResourcesManager.getInstance().camera, mPhysicsWorld, current_weapon.getDegat(), getDirection());
			
			s = bullet;
			break;
		case KNIFE:
			break;
		case ROCKET:
			break;
		default:
			break;
			
		}
		layer.attachChild(s);
	}
	
	
	public Weapon getWeapon(){
		return current_weapon;
	}
	
	public void setLayer(Entity pLayer){
		this.layer = pLayer;
	}
	
}
