package com.potatoe_team.potatoe_fight.manager;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.ui.IGameInterface.OnCreateSceneCallback;

import com.potatoe_team.potatoe_fight.map.MapXMLData;
import com.potatoe_team.potatoe_fight.scene.BaseScene;
import com.potatoe_team.potatoe_fight.scene.MainMenuScene;
//import com.potatoe_team.potatoe_fight.scene.GameScene;
import com.potatoe_team.potatoe_fight.scene.MainGameScene;
import com.potatoe_team.potatoe_fight.scene.menu.*;


/**
 * 
 * @author billy
 * @author Xiaoyi
 * @author Lionel
 * @version 1.0
 *
 */
public class SceneManager {

    //---------------------------------------------
    // SCENES
    //---------------------------------------------
    
    private BaseScene splashScene;
    private BaseScene menuScene;
    private BaseScene multiScene;
    private BaseScene gameScene;
    private BaseScene loadingScene;
    private BaseScene singleScene;
    private BaseScene aboutScene;
    private BaseScene optionScene;
    private BaseScene profilScene;
    
    //---------------------------------------------
    // VARIABLES
    //---------------------------------------------
    
    private static final SceneManager INSTANCE = new SceneManager();
    
    private SceneType currentSceneType = SceneType.SCENE_SPLASH;
    
    private BaseScene currentScene;
    //ajout
   // private BaseScene nextScene;
    //private int mNumFramesPassed;
    
   // private IUpdateHandler mLoadingScreenHandler;
    //fin d'ajout
    
    
    private Engine engine = ResourcesManager.getInstance().engine;
    
    public enum SceneType {
    	
        SCENE_SPLASH,
        SCENE_MENU,
        SCENE_GAME,
        SCENE_LOADING,
        SCENE_PROFIL,
        SCENE_ABOUT,
        SCENE_MULTI, SCENE_SINGLE,
        SCENE_OPTIONS,
    }
    
    //---------------------------------------------
    // CLASS LOGIC
    //---------------------------------------------
    
    public void setScene(BaseScene scene) {
    	
        engine.setScene(scene);
        currentScene = scene;
        currentSceneType = scene.getSceneType();
    }
    
    public void setScene(SceneType sceneType) {
    	
        switch (sceneType) {
        
            case SCENE_MENU:
                setScene(menuScene);
                break;
            case SCENE_GAME:
                setScene(gameScene);
                break;
            case SCENE_SPLASH:
                setScene(splashScene);
                break;
            case SCENE_LOADING:
                setScene(loadingScene);
                break;
            case SCENE_MULTI:
            	setScene(multiScene);
            case SCENE_SINGLE:
            	setScene(singleScene);
            case SCENE_ABOUT:
            	setScene(aboutScene);
            case SCENE_OPTIONS:
            	setScene(optionScene);
            case SCENE_PROFIL:
            	setScene(profilScene);	
            default:
                break;
        }
    }
    
    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------
    
    public static SceneManager getInstance() {
    	
        return INSTANCE;
    }
    
    public SceneType getCurrentSceneType() {
    	
        return currentSceneType;
    }
    
    public BaseScene getCurrentScene() {
    	
        return currentScene;
    }
    
    public void createSplashScene(OnCreateSceneCallback pOnCreateSceneCallback) {
    	
        ResourcesManager.getInstance().loadSplashScreen();
        splashScene = new SplashScene();
        currentScene = splashScene;
        pOnCreateSceneCallback.onCreateSceneFinished(splashScene);
    }
    
    public void disposeSplashScene() {
    	if(splashScene != null){
	        ResourcesManager.getInstance().unloadSplashScreen();
	        splashScene.disposeScene();
	        splashScene = null;
    	}
    }
    
    public void createMenuScene() {
    	
        ResourcesManager.getInstance().loadMenuResources();
        menuScene = new MainMenuScene();
        //loadingScene = new LoadingScene();
        SceneManager.getInstance().setScene(menuScene);
        //disposeSplashScene();
    }

    public void createMultiPlayerMenuScene() {
    	
          ResourcesManager.getInstance().loadMultiMenuResources();
          multiScene = new MultiPlayersMenuScene();
          setScene(multiScene);
          disposeMenuScene();   
    }
    
    public void createAboutMenuScene()
    {
          ResourcesManager.getInstance().loadAboutMenuResources();
          aboutScene = new AboutScene();
          setScene(aboutScene);
          disposeMenuScene();   
    }
    public void createOptionsMenuScene()
    {
          ResourcesManager.getInstance().loadOptionsMenuResources();
          optionScene = new OptionsScene();
          setScene(optionScene);
          disposeMenuScene();   
    }
    public void createProfilScene()
    {
          ResourcesManager.getInstance().loadProfilMenuResources();
          profilScene = new ProfilScene();
          setScene(profilScene);
          disposeMenuScene();   
    }
     public void onUpdate(IUpdateHandler mLoadingScreenHandler){
    	 //todo waits one frame before it unloads the previous scene 
    	 
     }
     
     public void showScene() {
    	 
     }
     public void showLayer() {
    	 
     }
     public void hideLayer() {
    	 
     }
    //fin ajout
    
    
     public void createGameScene(String map_name) {
    	this.createGameScene(MapXMLData.search_map_id(map_name));
     } 
     
    public void createGameScene(int map_id) {
    	disposeMenuScene();
    	ResourcesManager.getInstance().loadGameResources(map_id);
    	gameScene = new MainGameScene();
        setScene(gameScene);
    }
    
    // just for future, reset it to private
    public void disposeGameScene() {
    	
        ResourcesManager.getInstance().unloadGameScreen();
        gameScene.disposeScene();
        gameScene = null;
    }
    
    
    public void disposeMenuScene() {
    	
    	ResourcesManager.getInstance().unloadMenu();
    	menuScene.disposeScene();
    	menuScene = null;
    }
    public void disposeAboutScene(){
    	ResourcesManager.getInstance().unloadAboutMenu();
    	aboutScene.disposeScene();
    	aboutScene = null;
    }
    public void disposeOptionsScene(){
    	ResourcesManager.getInstance().unloadOptionsMenu();
    	optionScene.disposeScene();
    	optionScene = null;
    }
    public void disposeProfilScene(){
    	ResourcesManager.getInstance().unloadProfilMenu();
    	profilScene.disposeScene();
    	profilScene = null;
    }

	public void backToMenuScene() {
        SceneManager.getInstance().createMenuScene();
	}
	
}


