package com.potatoe_team.potatoe_fight.manager;

import org.andengine.util.debug.Debug;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.widget.Toast;

import com.potatoe_team.potatoe_fight.MainActivity;
import com.potatoe_team.potatoe_fight.connectivity.DeviceListFragment.DeviceActionListener;
import com.potatoe_team.potatoe_fight.connectivity.WiFiDirectBroadcastReceiver;

public class ConnectivityManager implements ChannelListener, DeviceActionListener{
	
	private static final ConnectivityManager INSTANCE = new ConnectivityManager();

	public WifiP2pManager wifi_manager;
	public Channel channel;
	public BroadcastReceiver broadcast_receiver;
	public IntentFilter intent_filter;
	
	private boolean isWifiP2PEnabled = false;
	private boolean retryChannel = false;
	

	public static void prepareManager(MainActivity activity) {
    	
        getInstance().wifi_manager = (WifiP2pManager) activity.getSystemService(Context.WIFI_P2P_SERVICE);
        getInstance().channel = getInstance().wifi_manager.initialize(activity, activity.getMainLooper(), null);
        getInstance().broadcast_receiver = new WiFiDirectBroadcastReceiver();
        getInstance().intent_filter = new IntentFilter();
        getInstance().intent_filter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        getInstance().intent_filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        getInstance().intent_filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        getInstance().intent_filter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
    }
	
	public void setIsWifiP2pEnabled(boolean pIsWifiP2pEnabled) {
		this.isWifiP2PEnabled = pIsWifiP2pEnabled;
	}
	
	public static ConnectivityManager getInstance() {
	    	
		return INSTANCE;
	}
	
	public void resetData() {
	/*        DeviceListFragment fragmentList = (DeviceListFragment) ResourcesManager.getInstance().activity.getFragmentManager().findFragmentById(R.id.frag_list);

	        if (fragmentList != null) {
	            fragmentList.clearPeers();
	        }
	 */
    }

	@Override
	public void showDetails(WifiP2pDevice device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelDisconnect() {

        /*
         * A cancel abort request by user. Disconnect i.e. removeGroup if
         * already connected. Else, request WifiP2pManager to abort the ongoing
         * request
         */
        if (wifi_manager != null) {
            /*
            final DeviceListFragment fragment = (DeviceListFragment) ResourcesManager.getInstance().activity.getFragmentManager().findFragmentById(R.id.frag_list);
            if (fragment.getDevice() == null
                    || fragment.getDevice().status == WifiP2pDevice.CONNECTED) {
                disconnect();
            } else if (fragment.getDevice().status == WifiP2pDevice.AVAILABLE
                    || fragment.getDevice().status == WifiP2pDevice.INVITED) {

                wifi_manager.cancelConnect(channel, new ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(ResourcesManager.getInstance().activity, "Aborting connection",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(ResourcesManager.getInstance().activity,
                                "Connect abort request failed. Reason Code: " + reasonCode,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            */
        }

    }
		

	@Override
	public void connect(WifiP2pConfig config) {

        wifi_manager.connect(channel, config, new ActionListener() {

            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(ResourcesManager.getInstance().activity, "Connect failed. Retry.", Toast.LENGTH_SHORT).show();
            }
        });
		
	}

	@Override
	public void disconnect() {

        wifi_manager.removeGroup(channel, new ActionListener() {

            @Override
            public void onFailure(int reasonCode) {
                Debug.e("potato", "Disconnect failed. Reason :" + reasonCode);

            }

            @Override
            public void onSuccess() {

            }

        });
		
	}

	@Override
	public void onChannelDisconnected() {

        if (wifi_manager != null && !retryChannel) {
            Toast.makeText(ResourcesManager.getInstance().activity, "Channel lost. Trying again", Toast.LENGTH_LONG).show();
            resetData();
            retryChannel = true;
            wifi_manager.initialize(ResourcesManager.getInstance().activity, ResourcesManager.getInstance().activity.getMainLooper(), this);
        } else {
            Toast.makeText(ResourcesManager.getInstance().activity,
                    "Severe! Channel is probably lost premanently. Try Disable/Re-Enable P2P.",
                    Toast.LENGTH_LONG).show();
        }
		
	}

}
