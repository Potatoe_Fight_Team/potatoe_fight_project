package com.potatoe_team.potatoe_fight.manager;

import java.util.Random;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.StrokeFont;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import android.graphics.Typeface;

import com.potatoe_team.potatoe_fight.MainActivity;
import com.potatoe_team.potatoe_fight.map.Map;
import com.potatoe_team.potatoe_fight.map.MapXMLData;


/**
 * 
 * @author billy
 * @author Lionel
 * @version 1.0
 */
public class ResourcesManager {
    //---------------------------------------------
    // VARIABLES
    //---------------------------------------------
    
    private static final ResourcesManager INSTANCE = new ResourcesManager();
    
    public Engine engine;
    public MainActivity activity;
    public SmoothCamera camera;
    public VertexBufferObjectManager vbom;
    public Font font;
    public Font HUD_font;
    
    public Map map;
    
    private BitmapTextureAtlas splashTextureAtlas;
    private BuildableBitmapTextureAtlas menuTextureAtlas;
    private BuildableBitmapTextureAtlas menuMultiTextureAtlas;
    private BuildableBitmapTextureAtlas gameTextureAtlas;
    private BitmapTextureAtlas mapTextureAtlas;
    private BuildableBitmapTextureAtlas minimapTextureAtlas;
    private BitmapTextureAtlas controlTextureAtlas;
    private BuildableBitmapTextureAtlas actionTextureAtlas;
    private BuildableBitmapTextureAtlas explosionTextureAtlas;
    private BuildableBitmapTextureAtlas HUDFlagTextureAtlas;
    private BitmapTextureAtlas HUDButtonTextureAtlas;
    private BitmapTextureAtlas HUDMinimapIconTextureAtlas;
    private BitmapTextureAtlas HUDWeaponSelectionIconTextureAtlas;
    private BuildableBitmapTextureAtlas aboutTextureAtlas;
    private BuildableBitmapTextureAtlas optionsTextureAtlas;
    private BuildableBitmapTextureAtlas profilTextureAtlas;
    private BuildableBitmapTextureAtlas profilTextureAtlas2;
    

    public ITextureRegion splash_region;
    
    public ITexture mParallaxLayerBackTexture;
    
    public ITextureRegion menu_background_region;
    public ITextureRegion play_region;
    public ITextureRegion titre_region;
    public ITextureRegion settings_region;
    public ITextureRegion single_region;
    public ITextureRegion return_region;
    public ITextureRegion options_region;
    public ITextureRegion button_region;
    public ITextureRegion no_sound_region;
    public ITextureRegion sound_region;
    
    public ITextureRegion droit_region;
	public ITextureRegion gauche_region;
	public ITextureRegion potatoe1_region;
	public ITextureRegion potatoe2_region;
	
	//public ArrayList<Profil> profil_region=new ArrayList<Profil>();
    //public ArrayList<ITextureRegion> profil_region=new ArrayList<ITextureRegion>();
	public ITextureRegion menu_multi_background_region;
	
	public TextureRegion game_background_region;
    public ITiledTextureRegion patate_region;
    
	public TextureRegion control_Base_Region;
	public TextureRegion control_Button_Region;
	public ITiledTextureRegion action_Button_Region;
	public ITiledTextureRegion wind_flag_storm;
	public ITiledTextureRegion explosion_Region;
	public TextureRegion button_setting;
	public TextureRegion minimap_icon;

    public TextureRegion game_midground_region;

	public StrokeFont HUD_healthBar_font;
	public StrokeFont HUD_Score_font;
	public StrokeFont HUD_Time_font;
	public StrokeFont HUD_Button_font;

	public ITextureRegion allies_indicator_texture_region;
	public ITextureRegion enemies_indicator_texture_region;
	public ITextureRegion player_indicator_texture_region;
	
	public TextureRegion weapon_selection_icon;
	public BitmapTextureAtlas weapon_selection_texture_atlas;
	public TextureRegion weapon_selection_right;
	public TextureRegion weapon_selection_left;
	public BitmapTextureAtlas weapon_selection_button_right_texture_atlas;
	public BitmapTextureAtlas weapon_selection_button_left_texture_atlas;
	public TextureRegion weapon_selection_frame;
    public BitmapTextureAtlas weapon_list_texture_atlas;
    public BitmapTextureAtlas weapon_list_texture_atlas2;
    public BitmapTextureAtlas weapon_list_texture_atlas3;
    public TextureRegion arme1;
    public TextureRegion arme2;
    public TextureRegion arme3;

	public TextureRegion about_region;

	public TiledTextureRegion arme_region;

	public TiledTextureRegion bullet_region;

	private BuildableBitmapTextureAtlas gunTextureAtlas;
	

	

	


    //---------------------------------------------
    // TEXTURES & TEXTURE REGIONS
    //---------------------------------------------
    
    //---------------------------------------------
    // CLASS LOGIC
    //---------------------------------------------

    public void loadMenuResources() {
    	
        loadMenuGraphics();
        loadMenuFonts();
        loadMenuAudio();
    }
    public void unloadMenuTextures() {
    	
        menuTextureAtlas.unload();
    }
        
    public void loadMenuTextures() {
    	
        menuTextureAtlas.load();
    }
    
    
    public void loadGameResources(int map_id) {
    	if(map_id == -1){
    		int[] id_list = MapXMLData.maps_id_list();
    		Random r = new Random();
    		int i = r.nextInt(id_list.length);
    		map = MapXMLData.load_mapXMLData(id_list[i]);
    	}else{
    		map = MapXMLData.load_mapXMLData(map_id);
    	}
        loadGameGraphics();
        loadGameFonts();
        loadGameAudio();
    }
    
    public void loadMapTexture(String texture_path){
    	
    }
    
 private void loadMenuGraphics() {
    	

    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
    	menuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	menu_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "menu_background.png");
    	play_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "button_play.png");
    	settings_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "button_setting.png");
    	button_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "button_potaeto.png");
    	about_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "button_about.png");   	
    	optionsTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	sound_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "button_sound.png");
    	no_sound_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "button_no_sound.png");
    	optionsTextureAtlas.load();
    	
    	try {
    		
    	    this.menuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
    	    //this.menuTextureAtlas.load();
    	    loadMenuTextures();
    	} catch (final TextureAtlasBuilderException e) {Debug.e(e);}
    }
   

    private void loadMenuFonts()
    {
    	BitmapTextureAtlas fontTexture = new BitmapTextureAtlas(engine.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
    	font = FontFactory.createStroke(engine.getFontManager(), fontTexture, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 20, true, Color.WHITE_ABGR_PACKED_INT, 0, Color.BLACK_ABGR_PACKED_INT);
    	font.load();
    }
    
    private void loadMenuAudio() {
        
    }
    
    public void unloadMenu() {
    	
    	menuTextureAtlas.unload();
    	menu_background_region = null;
    	//play_region = null;
    	button_region = null;
    }

    private void loadGameGraphics() {
        
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/game/");
        explosionTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 480, 160, TextureOptions.BILINEAR);
    	mapTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.REPEATING_BILINEAR);
    	gameTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	gunTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	explosion_Region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(explosionTextureAtlas, activity, "explosion.png", 3, 1);
    	game_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, map.background_texture_path);
      	game_midground_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mapTextureAtlas, activity, map.ground_texture_path, 0, 0);
      	if(this.activity.getPictureID() == 2) {
      		patate_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gameTextureAtlas, activity, "potato2.png",3,1);
      	}
      	else {
      		patate_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gameTextureAtlas, activity, "potato1.png",3,1);
      	}
      	arme_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gunTextureAtlas, activity, "gun.png",1,1);
    	bullet_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gunTextureAtlas, activity, "bullet.png",1,1);
       	try {
       		this.mapTextureAtlas.load();
       		this.explosionTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
        	this.explosionTextureAtlas.load();
    	    this.gameTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
    	    this.gameTextureAtlas.load();
    	    this.gunTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
    	    this.gunTextureAtlas.load();
    	} catch (final TextureAtlasBuilderException e) { Debug.e(e);}
       	
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/control/");
        controlTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        actionTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 128, 64, TextureOptions.BILINEAR);
        control_Base_Region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(controlTextureAtlas, activity, "control_base.png", 0, 0);
        control_Button_Region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(controlTextureAtlas, activity, "control_joystick.png", 128, 0);
        action_Button_Region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(actionTextureAtlas, activity, "action_button.png", 2, 1);
        this.controlTextureAtlas.load();
        try{
        	this.actionTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
        	this.actionTextureAtlas.load();
        }catch (final TextureAtlasBuilderException e) { Debug.e(e);}
        
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/HUD/");
        HUDFlagTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 320, 32, TextureOptions.BILINEAR);
        minimapTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 32, 32, TextureOptions.BILINEAR);
        HUDMinimapIconTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 32, 32, TextureOptions.BILINEAR);
        HUDWeaponSelectionIconTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 32, 32, TextureOptions.BILINEAR);
        HUDButtonTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 64, 64, TextureOptions.BILINEAR);
        weapon_selection_texture_atlas  = new BitmapTextureAtlas(activity.getTextureManager(), 400, 112, TextureOptions.BILINEAR);
        weapon_selection_button_right_texture_atlas  = new BitmapTextureAtlas(activity.getTextureManager(), 400, 112, TextureOptions.BILINEAR);
        weapon_selection_button_left_texture_atlas  = new BitmapTextureAtlas(activity.getTextureManager(), 400, 112, TextureOptions.BILINEAR);

        minimap_icon = BitmapTextureAtlasTextureRegionFactory.createFromAsset(HUDMinimapIconTextureAtlas, activity, "minimap_icon.png", 0, 0);
        weapon_selection_icon = BitmapTextureAtlasTextureRegionFactory.createFromAsset(HUDWeaponSelectionIconTextureAtlas, activity, "weapon_selector_icon.png", 0, 0);
        button_setting = BitmapTextureAtlasTextureRegionFactory.createFromAsset(HUDButtonTextureAtlas, activity, "button_setting.png", 0, 0);
        wind_flag_storm = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(HUDFlagTextureAtlas, activity, "WindFlagStorm.png", 5, 1);
        enemies_indicator_texture_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(minimapTextureAtlas, activity, "enemy_pinpoint.png");
        allies_indicator_texture_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(minimapTextureAtlas, activity, "ally_pinpoint.png");
        player_indicator_texture_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(minimapTextureAtlas, activity, "player_pinpoint.png");
        weapon_selection_right = BitmapTextureAtlasTextureRegionFactory.createFromAsset(weapon_selection_button_left_texture_atlas, activity, "right.png", 0, 0);
        weapon_selection_left = BitmapTextureAtlasTextureRegionFactory.createFromAsset(weapon_selection_button_right_texture_atlas, activity, "left.png", 0, 0);
        weapon_selection_frame = BitmapTextureAtlasTextureRegionFactory.createFromAsset(weapon_selection_texture_atlas, activity, "frame.png", 0, 0);
        weapon_list_texture_atlas = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
        weapon_list_texture_atlas2 = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
        weapon_list_texture_atlas3 = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
        arme1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(weapon_list_texture_atlas, activity, "sprite1.png", 0, 0);
        arme2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(weapon_list_texture_atlas2, activity, "sprite2.png", 0, 0);
        arme3 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(weapon_list_texture_atlas3, activity, "sprite3.png", 0, 0);
        try{
        	this.HUDFlagTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
        	this.HUDFlagTextureAtlas.load();
        	this.HUDButtonTextureAtlas.load();
        	this.minimapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
        	this.minimapTextureAtlas.load();
        	this.HUDMinimapIconTextureAtlas.load();
        	this.HUDWeaponSelectionIconTextureAtlas.load();
        	this.weapon_selection_texture_atlas.load();
    		this.weapon_selection_button_right_texture_atlas.load();
    		this.weapon_selection_button_left_texture_atlas.load();
    		this.weapon_list_texture_atlas.load();
    		this.weapon_list_texture_atlas2.load();
    		this.weapon_list_texture_atlas3.load();
        }catch (final TextureAtlasBuilderException e) { Debug.e(e);}

    }
    
    
    private void loadGameFonts() {
        BitmapTextureAtlas fontTexture1 = new BitmapTextureAtlas(engine.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        HUD_healthBar_font = FontFactory.createStroke(engine.getFontManager(), fontTexture1, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 18, true, Color.BLACK_ABGR_PACKED_INT, 1, Color.WHITE_ABGR_PACKED_INT);
        HUD_healthBar_font.load();
        BitmapTextureAtlas fontTexture2 = new BitmapTextureAtlas(engine.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        HUD_Score_font = FontFactory.createStroke(engine.getFontManager(), fontTexture2, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 16, true, Color.BLACK_ABGR_PACKED_INT, 0, Color.WHITE_ABGR_PACKED_INT);
        HUD_Score_font.load();
        BitmapTextureAtlas fontTexture3 = new BitmapTextureAtlas(engine.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        HUD_Time_font = FontFactory.createStroke(engine.getFontManager(), fontTexture3, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 16, true, Color.BLACK_ABGR_PACKED_INT, 0, Color.WHITE_ABGR_PACKED_INT);
        HUD_Time_font.load();
        BitmapTextureAtlas fontTexture4 = new BitmapTextureAtlas(engine.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        HUD_Button_font = FontFactory.createStroke(engine.getFontManager(), fontTexture4, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 32, true, Color.WHITE_ABGR_PACKED_INT, 1, Color.BLACK_ABGR_PACKED_INT);
        HUD_Button_font.load();
    }
    
    private void loadGameAudio() {
        
    }
    
    public void unloadGameScreen() {
    	
    	gameTextureAtlas.unload();
    	game_background_region = null;
    	game_midground_region = null;
    }
    
    public void loadSplashScreen() {
    	
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
    	splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, "splash.png", 0, 0);
    	splashTextureAtlas.load();
    }
    
    public void unloadSplashScreen() {
    	
    	splashTextureAtlas.unload();
    	splash_region = null;
    }
    
public void unloadMenuRessources() {
    	
    	menuTextureAtlas.unload();
    	menu_background_region = null;
    	button_region = null;
    	//return_region = null;
    }
    
    public void loadMultiMenuResources() {
    	
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
    	menuMultiTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	menu_multi_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuMultiTextureAtlas, activity, "splash.png");
    	play_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "play.png");
    	settings_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "options.png");
    	
    	try {
    	    
    		this.menuMultiTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
    	    this.menuMultiTextureAtlas.load();
    	} catch (final TextureAtlasBuilderException e) { Debug.e(e); }
    }
    public void loadAboutMenuResources(){
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
    	aboutTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	menu_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(aboutTextureAtlas, activity, "menu_about.png");
    	return_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(aboutTextureAtlas, activity, "button_back.png");
    	
    	//settings_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "options.png");
    	try 
    	{
    	    this.aboutTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
    	    this.aboutTextureAtlas.load();
    	} 
    	
    	catch (final TextureAtlasBuilderException e)
    	{
    	        Debug.e(e);
    	}
    }
    public void unloadAboutMenu() {
		aboutTextureAtlas.unload();
    	menu_background_region = null;
    	button_region = null;
		
	}
    
    
    public void loadOptionsMenuResources(){
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
    	optionsTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	menu_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionsTextureAtlas, activity, "menu_background.png");
    	button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionsTextureAtlas, activity, "button_sound.png");
    	no_sound_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionsTextureAtlas, activity, "button_no_sound.png");
    	return_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(optionsTextureAtlas, activity, "button_back.png");
    	try 
    	{
    	    this.optionsTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
    	    this.optionsTextureAtlas.load();
    	} 
    	
    	catch (final TextureAtlasBuilderException e)
    	{
    	        Debug.e(e);
    	}
    }
    public void unloadOptionsMenu() {
		optionsTextureAtlas.unload();
    	menu_background_region = null;
    	button_region = null;
    	no_sound_region=null;
		
	}
    public void loadProfilMenuResources(){
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
    	profilTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	profilTextureAtlas2 = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
    	menu_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(profilTextureAtlas, activity, "menu_profile.png");
    	button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(profilTextureAtlas, activity, "button_back.png");
    	droit_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(profilTextureAtlas, activity, "droit.png");
    	gauche_region=BitmapTextureAtlasTextureRegionFactory.createFromAsset(profilTextureAtlas, activity, "gauche.png");
    	potatoe1_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(profilTextureAtlas, activity, "potatoe-1.png");
    	potatoe2_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(profilTextureAtlas2, activity, "potatoe-2.png");
    	try 
    	{
    	    this.profilTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
    	    this.profilTextureAtlas.load();
    	    this.profilTextureAtlas2.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
    	    this.profilTextureAtlas2.load();
    	} 
    	
    	catch (final TextureAtlasBuilderException e)
    	{
    	        Debug.e(e);
    	}
    }
    public void unloadProfilMenu() {
		profilTextureAtlas.unload();
    	menu_background_region = null;
    	droit_region = null;
    	gauche_region=null;
    	//profil_region.clear();
		
	}
    /**
     * @param engine
     * @param activity
     * @param camera
     * @param vbom
     * 
     * We use this method at beginning of game loading, to prepare Resources Manager properly,
     * setting all needed parameters, so we can latter access them from different classes (eg. scenes)
     */
    public static void prepareManager(Engine engine, MainActivity activity, SmoothCamera camera, VertexBufferObjectManager vbom) {
    	
        getInstance().engine = engine;
        getInstance().activity = activity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;
    }
    
    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------
    
    public static ResourcesManager getInstance() {
    	
        return INSTANCE;
    }
	
}

