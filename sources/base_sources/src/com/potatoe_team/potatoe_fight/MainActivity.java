package com.potatoe_team.potatoe_fight;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;
import org.apache.commons.io.IOUtils;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import android.content.Context;
import android.view.KeyEvent;

import com.potatoe_team.potatoe_fight.manager.ConnectivityManager;
import com.potatoe_team.potatoe_fight.manager.ResourcesManager;
import com.potatoe_team.potatoe_fight.manager.SceneManager;


/**
 * The main activity use in the beginning of App.
 * 
 * @author Billy
 * @author Alban
 * @author Xiaoy
 * @author Lionel
 * @author Guillaume
 * @version 1.0
 */
public class MainActivity extends BaseGameActivity {
	
    private org.jdom2.Document document;
	private Element racine;
	private int pictureID;
	/**
	 *  The following constants will be used to define the height and the width
	 *  of our game's camera view
	 */
	private static final int HEIGHT = 480;
	private static final int WIDTH = 800;
	private SmoothCamera camera;
	/**
	 * The onCreateEngineOptions method is responsible for creating the options
	 * to be applied to the Engine object once it is created. The options
	 * include, but are not limited to enabling/disable sounds and music,
	 * defining multitouch options, changing rendering options and more.
	 * 
	 * @return The EngineOptions created.
	 */
	@Override
	public EngineOptions onCreateEngineOptions() {
		
		final float maxCameraXVelocity = 4096.0f;
		final float maxCameraYVelocity = 4096.0f;
		final float maxZoomFactorChange = 5.0f;
		
		ConnectivityManager.prepareManager(this);
		
		// Define our mCamera object
		this.camera = new SmoothCamera(0, 0, WIDTH, HEIGHT, maxCameraXVelocity, maxCameraYVelocity, maxZoomFactorChange);

		// Declare & Define our engine options to be applied to our Engine
		// object
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), this.camera);
		
		//used to enable the dither effect (limited color banding in images)
		engineOptions.getRenderOptions().setDithering(true);
		engineOptions.getRenderOptions().getConfigChooserOptions().setRequestedMultiSampling(true);
		//used to enable multitouch
		engineOptions.getTouchOptions().setNeedsMultiTouch(true);
		
		// It is necessary in a lot of applications to define the following
		// wake lock options in order to disable the device's display
		// from turning off during gameplay due to inactivity
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);

		engineOptions.getAudioOptions().setNeedsMusic(false);
		engineOptions.getAudioOptions().setNeedsSound(false);
		//engineOptions.getAudioOptions().getSoundOptions().setMaxSimultaneousStreams(2);
		// Return the engineOptions object, passing it to the engine
		return engineOptions;
	}
	
	/**
	 * The onCreateEngine method can be use to choose which type of engine to use
	 * between Engine(EngineOptions) default game engine with unlimited FPS
	 * FixedStepEngine(EngineOptions, int) engine that work at a fixed number of steps by second
	 * LimitedFPSEngine(EngineOptions, int) engine that can't run faster than a fixed max FPS
	 * 
	 * @param pEngineOptions Options use to create Engine
	 * @return The Engine created.
	 */
	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions) {
		
		// Create a fixed step engine, which will run at a maximum of 60 steps
		return new FixedStepEngine(pEngineOptions, 30);
	}

	/**
	 * The onCreateResources method is in place for resource loading, including
	 * textures, sounds, and fonts for the most part.
	 */
	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException{
		pictureID = getProfilePicture();
		//ResourcesManager initialization
		ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
		/*
		 * We should notify the pOnCreateResourcesCallback that we've finished
		 * loading all of the necessary resources in our game AFTER they are
		 * loaded. onCreateResourcesFinished() should be the last method called.
		 */
		
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	/**
	 * The onCreateScene method is in place to handle the scene initialization
	 * and setup. In this method, we must at least *return our mScene object*
	 * which will then be set as our main scene within our Engine object
	 * (handled "behind the scenes"). This method might also setup touch
	 * listeners, update handlers, or more events directly related to the scene.
	 * 
	 * @param pOnCreateSceneCallback Method use to callback at the end of creating scene.
	 */
	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws IOException{
		
		SceneManager.getInstance().createSplashScene(pOnCreateSceneCallback);
	}

	/**
	 * The onPopulateScene method was introduced to AndEngine as a way of
	 * separating scene-creation from scene population. This method is in place
	 * for attaching child entities to the scene once it has already been
	 * returned to the engine and set as our main scene.
	 * 
	 * @param pScene The Scene to populate.
	 * @param pOnPopulateSceneCallback Method use to callback at the end of populating scene.
	 */
	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException{
		
		 mEngine.registerUpdateHandler(new TimerHandler(1f, new ITimerCallback() {
			 
			 public void onTimePassed(final TimerHandler pTimerHandler) 
		     {
				 mEngine.unregisterUpdateHandler(pTimerHandler);
				 SceneManager.getInstance().createMenuScene();
		     }
		}));
		pOnPopulateSceneCallback.onPopulateSceneFinished();		    
	}
	
	/**
	 * Method call on destroy
	 */
	@Override
	protected void onDestroy() {
		writeProfilePicture();
		super.onDestroy();
	    System.exit(0);	
	}
	
	/**
	 * Method call on KeyDown
	 * Why do we use keyCode instead of event.getKeyCode() ?
	 * @param keyCode the code of key pressed.
	 * @param event	The event that call the method.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {  
		
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	
	        SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
	    }
	    
	    return false; 
	}

	@Override
	protected void onResume() {
	    super.onResume();
	    this.registerReceiver(ConnectivityManager.getInstance().broadcast_receiver, ConnectivityManager.getInstance().intent_filter);
	}
	
	@Override
	protected void onPause() {
	    super.onPause();
	    this.unregisterReceiver(ConnectivityManager.getInstance().broadcast_receiver);
	}

    
	public int getProfilePicture() {
		
		String res ="";
		
		SAXBuilder sxb = new SAXBuilder();
	    
		try
	    {
	         document = sxb.build(openFileInput("playersdata.xml"));
	         racine = document.getRootElement();

	         Element user = racine.getChild("potato");
	         res = user.getChild("picture").getText();
	         
	         return Integer.parseInt(res);
	    }
		catch(FileNotFoundException e) {
			
			try {
				FileOutputStream output = openFileOutput("playersdata.xml", Context.MODE_PRIVATE);

				output.write(IOUtils.toByteArray(getAssets().open("data/player/playersdata.xml")));
				
				output.close();
				
				document = sxb.build(openFileInput("playersdata.xml"));
		        racine = document.getRootElement();

		        Element user = racine.getChild("potato");
		        res = user.getChild("picture").getText();
		         
		        return Integer.parseInt(res);
		         
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	    catch(Exception e){}
		   
		return 2;
	}
	
	public void writeProfilePicture() {
		
		Element user = racine.getChild("potato");
		
        user.getChild("picture").setText(""+ pictureID);
        
        XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
        try {
			sortie.output(document, openFileOutput("playersdata.xml", Context.MODE_PRIVATE));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getPictureID() {
		return pictureID;
	}
	
	public void setPictureID(int x) {
		pictureID = x;
	}

}